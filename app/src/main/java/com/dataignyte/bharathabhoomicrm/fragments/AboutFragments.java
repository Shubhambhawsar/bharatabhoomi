package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.dataignyte.bharathabhoomicrm.BuildConfig;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutFragments extends BaseFragment {

    @BindView(R.id.txvVersionName)
    TextView txvVersionName;
    @BindView(R.id.txvLastUpdate)
    TextView txvLastUpdate;

    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        txvVersionName.setText(BuildConfig.VERSION_NAME);
    }

    @OnClick({R.id.txvTermsOfServices, R.id.txvButtonBackToHome})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer)getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;

            case R.id.txvTermsOfServices:
                Utility.ShowToastMessage(context, "Upcoming soon");
                break;
        }
    }

}
