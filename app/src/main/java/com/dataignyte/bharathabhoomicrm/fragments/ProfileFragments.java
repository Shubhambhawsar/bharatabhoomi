package com.dataignyte.bharathabhoomicrm.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.Glide;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.BaseActivity;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.FileUploadResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetProfileResponse;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ProfileFragments extends BaseFragment {

    private static final String TAG = ProfileFragments.class.getSimpleName();
    @BindView(R.id.ivUserPhoto)
    ImageView ivUserPhoto;
    @BindView(R.id.txvFirstName)
    EditText txvFirstName;
    @BindView(R.id.txvLastName)
    EditText txvLastName;
    @BindView(R.id.txvEmail)
    EditText txvEmail;
    @BindView(R.id.txvMobileNumber)
    EditText txvMobileNumber;
    @BindView(R.id.txvUserRole)
    EditText txvUserRole;
    @BindView(R.id.txvAddress)
    EditText txvAddress;
    private Context context;
    private File uploadfile;
    private String uploadfilePath = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        getUserProfileDetailsAPI();
    }

    @OnClick({R.id.txvButtonBackToHome, R.id.cvBtnAdd, R.id.ivUserPhoto})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer)getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;
            case R.id.cvBtnAdd:
            case R.id.ivUserPhoto:
                if (ContextCompat.checkSelfPermission(context, CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {context.getString(R.string.from_camera), context.getString(R.string.from_gallery), context.getString(R.string.close)};
                    Utility.selectimag(context, ProfileFragments.this, context.getString(R.string.add_photo), options);
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, Constants.PermissionCodes.PERMISSION_REQUEST_CODE);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PermissionCodes.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {context.getString(R.string.from_camera), context.getString(R.string.from_gallery), context.getString(R.string.close)};
                    Utility.selectimag(context, ProfileFragments.this, context.getString(R.string.add_photo), options);
                } else {
                    Toast.makeText(context, "permission denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            File file = null;
            switch (requestCode) {
                case Constants.PermissionCodes.CAMERA__REQUEST_CODE:
                    uploadfilePath = Utility.imageFilePath;
                    file = new File(uploadfilePath);
                    uploadfile = file;
                    Glide.with(context).load(Utility.imageFilePath).into(ivUserPhoto);
                    addUserProfilePictureAPI();
                    break;

                case Constants.PermissionCodes.GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    try {
                        uploadfilePath = Utility.getFilePath(context, selectedImage);
                        file = new File(uploadfilePath);
                        uploadfile = file;
                        Glide.with(context).load(selectedImage).into(ivUserPhoto);
                        addUserProfilePictureAPI();
                    } catch (Exception e) {
                        Log.e(TAG, "onActivityResult: " + e.getMessage());
                    }
                    break;
            }
        }
    }

    private void getUserProfileDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getUserProfileDetailsAPI(object).enqueue(new Callback<GetProfileResponse>() {
                @Override
                public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body() != null && response.body().getFirst_name() != null) {
                            txvFirstName.setText(Utility.geNonNullString(response.body().getFirst_name()));
                            txvLastName.setText(Utility.geNonNullString(response.body().getLast_name()));
                            txvEmail.setText(Utility.geNonNullString(response.body().getUser_email()));
                            txvMobileNumber.setText(Utility.geNonNullString(response.body().getUser_phone()));
                            txvUserRole.setText(Utility.geNonNullString(response.body().getUser_role()));
                            txvAddress.setText(Utility.geNonNullString(response.body().getUser_address()));

                            if (Utility.geNonNullString(response.body().getProfile_pic_url()).length()>10) {
                                Glide.with(context).load(response.body().getProfile_pic_url()).into(ivUserPhoto);
                                SharedPref.setSharedPreference(context, Constants.PROFILE_PIC, response.body().getProfile_pic_url());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getDashBoardAgentList onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void addUserProfilePictureAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(Constants.CUSTOMER_ID, SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID));
            builder.addFormDataPart(Constants.USER_EMAIL, SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            if (uploadfile != null) {
                builder.addFormDataPart(Constants.PROFILE_PIC, uploadfile.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), uploadfile));
            }

            MultipartBody requestBody = builder.build();

            RetrofitClient.getAPIService().addUserProfilePictureAPI(requestBody).enqueue(new Callback<FileUploadResponse>() {
                @Override
                public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body().getSuccess() != null)
                            Utility.ShowToastMessage(context, response.body().getSuccess());
                        else
                            Utility.ShowToastMessage(context, response.body().getError());
                    }
                }

                @Override
                public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                    Utility.ShowToastMessage(context, "Network problem! try again....");
                    stopProgressHud(context);
                    Log.e(TAG, "addUserProfilePictureAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}
