package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.adapters.LeadChatMessagAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.SendMessageRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.ConversationMessage;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupportFragment extends BaseFragment {

    private static final String TAG = SupportFragment.class.getSimpleName();
    @BindView(R.id.chatListRV)
    RecyclerView chatListRV;
    @BindView(R.id.edtMessage)
    EditText edtMessage;
    private LeadChatMessagAdapter adapter;
    private ArrayList<ConversationMessage> conversationMessages = new ArrayList<>();

    private Context context;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_support, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        chatListRV.setLayoutManager(new LinearLayoutManager(context));
        adapter = new LeadChatMessagAdapter(context, conversationMessages, false);
        chatListRV.setAdapter(adapter);
        getSupportTeamMessagesAPI();
    }

    @OnClick({R.id.txvButtonBackToHome, R.id.ivButtonSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer)getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;

            case R.id.ivButtonSend:
                if (edtMessage.getText().toString().trim().length()!=0)
                    callSendMessagesAPI(edtMessage.getText().toString().trim());
                break;
        }
    }

    public void getSupportTeamMessagesAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getSupportTeamMessagesAPI(object).enqueue(new Callback<ArrayList<ConversationMessage>>() {
                @Override
                public void onResponse(Call<ArrayList<ConversationMessage>> call, Response<ArrayList<ConversationMessage>> response) {
                    stopProgressHud(context);
                    if (response!=null && response.body()!=null && response.body().size()!=0) {
                        conversationMessages = response.body();
                        adapter = new LeadChatMessagAdapter(context, conversationMessages, false);
                        chatListRV.setAdapter(adapter);
                        chatListRV.scrollToPosition((conversationMessages.size()-1));
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ConversationMessage>> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getSupportTeamMessagesAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void callSendMessagesAPI(String message) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            SendMessageRequest object = new SendMessageRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            object.setMessage(message);
            RetrofitClient.getAPIService().callSendQueryMessagesAPI(object).enqueue(new Callback<SuccessResponse>() {
                @Override
                public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                    stopProgressHud(context);
                    if (response.body()!=null && response.body().getSuccess()!=null) {
                        ConversationMessage object = new ConversationMessage();
                        object.setText(message);
                        object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
                        object.setUser_role(SharedPref.getSharedPreferences(context, Constants.USER_ROLE));
                        object.setUsername(SharedPref.getSharedPreferences(context, Constants.USER_NAME));
                        object.setTime(new SimpleDateFormat("dd-MMM-yyyy hh:mm: a").format(new Date()));
                        object.setDate(new SimpleDateFormat("dd-MMM-yyyy hh:mm: a").format(new Date()));
                        conversationMessages.add(object);
                        adapter.updateArrayList(conversationMessages);
                        if (conversationMessages.size()!=1)
                            chatListRV.scrollToPosition((conversationMessages.size()-1));
                        edtMessage.setText("");
                    }
                }

                @Override
                public void onFailure(Call<SuccessResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "callSendMessagesAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}
