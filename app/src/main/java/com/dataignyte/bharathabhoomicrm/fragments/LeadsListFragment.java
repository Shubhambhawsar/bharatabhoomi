package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.adapters.LeadsListAdapter;
import com.dataignyte.bharathabhoomicrm.helper.DragEventListener;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadsListFragment extends BaseFragment implements DragEventListener {

    private static final String TAG = LeadsListFragment.class.getSimpleName();
    @BindView(R.id.taskListRV)
    RecyclerView taskListRV;
    @BindView(R.id.txvNoTasksAdded)
    TextView txvNoTasksAdded;
    @BindView(R.id.recyclerView)
    RecyclerView leadListRV;
    @BindView(R.id.txvNoDataFound)
    TextView txvNoLeadsFound;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.cbTask)
    CheckBox cbTask;

    private Context context;
    private LeadsListAdapter leadListAdapter;
    private LeadsListAdapter taskListAdapter;
    private ArrayList<GetDashBoardResponse> leadList = new ArrayList<>();
    private ArrayList<GetDashBoardResponse> tasksList = new ArrayList<>();
    private Snackbar snackbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_lead, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        taskListRV.setLayoutManager(new LinearLayoutManager(context));
        leadListRV.setLayoutManager(new LinearLayoutManager(context));
        snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content), "Drag and move leads in task list", Snackbar.LENGTH_INDEFINITE);
        setSwipeToRefresh();
        setSearchTextChangeListener();

        tasksList = SharedPref.getSharePreferenceTasksList(context);
        if (tasksList == null)
            tasksList = new ArrayList<>();
        taskListAdapter = new LeadsListAdapter(context, tasksList, LeadsListFragment.this, true);
        taskListRV.setAdapter(taskListAdapter);


        if (tasksList==null || tasksList.size()==0)
            txvNoTasksAdded.setVisibility(View.VISIBLE);
        else
            txvNoTasksAdded.setVisibility(View.GONE);

        setCreateEditTaskCheckBoxListener();
    }

    private void setCreateEditTaskCheckBoxListener(){
        cbTask.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    snackbar.show();
                    if (taskListAdapter!=null) {
                        txvNoTasksAdded.setOnDragListener(taskListAdapter.getDragInstance());
                        taskListRV.setOnDragListener(taskListAdapter.getDragInstance());
                    }

                    if (leadListAdapter!=null) {
                        txvNoLeadsFound.setOnDragListener(leadListAdapter.getDragInstance());
                        leadListRV.setOnDragListener(leadListAdapter.getDragInstance());
                    }
                } else {
                    snackbar.dismiss();
                    txvNoTasksAdded.setOnDragListener(null);
                    taskListRV.setOnDragListener(null);

                    txvNoLeadsFound.setOnDragListener(null);
                    leadListRV.setOnDragListener(null);
                }

                if (taskListAdapter!=null) {
                    taskListAdapter.isTaskDragable = isChecked;
                    taskListAdapter.notifyDataSetChanged();
                }

                if (leadListAdapter!=null) {
                    leadListAdapter.isTaskDragable = isChecked;
                    leadListAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void setSwipeToRefresh() {
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                getDashBoardAgentList();
                swipeLayout.setRefreshing(false);
            }
        });
    }

    private void setSearchTextChangeListener() {
        MainActivitySideDrawer.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (leadListAdapter != null)
                    leadListAdapter.getFilter().filter(s.toString());
            }
        });
    }

    @OnClick({R.id.txvButtonBackToHome})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer) getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDashBoardAgentList();
    }

    private void getDashBoardAgentList() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getDashBoardLeadsList(object).enqueue(new Callback<ArrayList<GetDashBoardResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetDashBoardResponse>> call, Response<ArrayList<GetDashBoardResponse>> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body() != null && response.body().size() != 0) {
                            txvNoLeadsFound.setVisibility(View.GONE);
                            leadList = response.body();

                            tasksList = SharedPref.getSharePreferenceTasksList(context);
                            if (tasksList == null)
                                tasksList = new ArrayList<>();
                            if (tasksList.size()!=0 && leadList.size()!=0) {
                                for (int i=0; i< tasksList.size(); i++) {
                                    for (int j =0; j< leadList.size(); j++) {
                                        Log.e(TAG, "Lead id: "+ leadList.get(j).get_id()+ "\nTask id: "+tasksList.get(i).get_id());
                                        if (leadList.get(j).get_id().equalsIgnoreCase(tasksList.get(i).get_id()))
                                            leadList.remove(j);

                                    }
                                }
                            }

                            leadListAdapter = new LeadsListAdapter(context, leadList, LeadsListFragment.this, false);
                            leadListRV.setAdapter(leadListAdapter);
                        } else {
                            txvNoLeadsFound.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetDashBoardResponse>> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getDashBoardAgentList onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    @Override
    public void setEmptyListTop(boolean visibility) {
        if (visibility)
            SharedPref.saveTaskListInSharedPreference(context, new ArrayList<>());
        txvNoTasksAdded.setVisibility(visibility ? View.VISIBLE : View.GONE);
        taskListRV.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setEmptyListBottom(boolean visibility) {
        txvNoLeadsFound.setVisibility(visibility ? View.VISIBLE : View.GONE);
        leadListRV.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        snackbar.dismiss();
    }
}
