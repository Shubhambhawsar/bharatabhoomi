package com.dataignyte.bharathabhoomicrm.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.contentcapture.ContentCaptureCondition;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.adapters.LeadChatMessagAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.LeadAgentConversationRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.DocumentsResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.FileUploadResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadAgentConversationResponse;

import java.io.File;
import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentsFragment extends BaseFragment {

    private String TAG = DocumentsFragment.class.getSimpleName();
    @BindView(R.id.txvAadharCardUrl)
    TextView txvAadharCardUrl;
    @BindView(R.id.txvPanCardUrl)
    TextView txvPanCardUrl;
    @BindView(R.id.txvButtonViewAdhaar)
    TextView txvButtonViewAdhaar;
    @BindView(R.id.txvButtonViewPan)
    TextView txvButtonViewPan;

    private Context context;
    private String adharCardUrl = "", pancardUrl= "";
    private File aadharCard, panCard;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_documents, container, false);
        ButterKnife.bind(this, view);
        context = getContext();
        getDocumentsAPI();
        return view;
    }

    @OnClick({R.id.txvButtonBackToHome, R.id.txvButtonViewAdhaar, R.id.txvButtonViewPan, R.id.llBtnAadharCard, R.id.llBtnPanCard, R.id.txvButtonSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer)getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;

            case R.id.txvButtonViewAdhaar:
                if (adharCardUrl!=null && adharCardUrl.length()>11){
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(adharCardUrl));
                    startActivity(i);
                }
                break;
            case R.id.txvButtonViewPan:
                if (pancardUrl!=null && pancardUrl.length()>11){
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(pancardUrl));
                    startActivity(i);
                }
                break;
            case R.id.llBtnAadharCard:
                startActivityForResult(Utility.getFileChooserIntentForImageAndPdf(), Constants.PermissionCodes.AADHAR_CARD_REQUEST_CODE);
                break;

            case R.id.llBtnPanCard:
                startActivityForResult(Utility.getFileChooserIntentForImageAndPdf(), Constants.PermissionCodes.PAN_CARD_REQUEST_CODE);
                break;

            case R.id.txvButtonSave:
                if (aadharCard !=null)
                    addUserProfilePictureAPI();
                else if (panCard !=null)
                    addUserProfilePictureAPI();
                break;
        }
    }

    private void getDocumentsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getDocumentsAPI(object).enqueue(new Callback<DocumentsResponse>() {
                @Override
                public void onResponse(Call<DocumentsResponse> call, Response<DocumentsResponse> response) {
                    stopProgressHud(context);
                    if (response!=null && response.body()!=null) {
                        adharCardUrl = response.body().getAadharCardUrl();
                        pancardUrl = response.body().getPanCardUrl();
                        txvAadharCardUrl.setText(response.body().getAadharCardUrl());
                        txvPanCardUrl.setText(response.body().getPanCardUrl());

                        if (adharCardUrl!=null && adharCardUrl.length()>11)
                            txvButtonViewAdhaar.setVisibility(View.VISIBLE);
                        else
                            txvButtonViewAdhaar.setVisibility(View.GONE);

                        if (pancardUrl!=null && pancardUrl.length()>11)
                                txvButtonViewPan.setVisibility(View.VISIBLE);
                        else
                            txvButtonViewPan.setVisibility(View.GONE);


                    }
                }

                @Override
                public void onFailure(Call<DocumentsResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getLeadAgentConversationMessagesAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void addUserProfilePictureAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(Constants.CUSTOMER_ID, SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID));
            builder.addFormDataPart(Constants.USER_EMAIL, SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            if (aadharCard != null) {
                builder.addFormDataPart(Constants.ADHAAR, aadharCard.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), aadharCard));
            }

            if (panCard != null) {
                builder.addFormDataPart(Constants.PAN_CARD, panCard.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), panCard));
            }

            MultipartBody requestBody = builder.build();

            RetrofitClient.getAPIService().uploadDocumentAPI(requestBody).enqueue(new Callback<FileUploadResponse>() {
                @Override
                public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body().getSuccess() != null)
                            Utility.ShowToastMessage(context, response.body().getSuccess());
                        else
                            Utility.ShowToastMessage(context, response.body().getError());
                    }
                }

                @Override
                public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                    Utility.ShowToastMessage(context, "Network problem! try again....");
                    stopProgressHud(context);
                    Log.e(TAG, "addUserProfilePictureAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.PermissionCodes.AADHAR_CARD_REQUEST_CODE:
                    try {
                        aadharCard  = new File(Utility.getFilePath(context, data.getData()));
                        txvAadharCardUrl.setText(aadharCard.getName());
                    } catch (Exception e) {
                        Log.e(TAG, "onActivityResult: "+e.getMessage() );
                    }
                    break;

                case Constants.PermissionCodes.PAN_CARD_REQUEST_CODE:
                    try {
                        panCard  = new File(Utility.getFilePath(context, data.getData()));
                        txvPanCardUrl.setText(panCard.getName());
                    } catch (Exception e) {
                        Log.e(TAG, "onActivityResult: "+e.getMessage() );
                    }
                    break;
            }
        }
    }
}
