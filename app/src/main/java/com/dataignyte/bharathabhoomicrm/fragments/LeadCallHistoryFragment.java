package com.dataignyte.bharathabhoomicrm.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.LeadCallHistoryAdapter;
import com.dataignyte.bharathabhoomicrm.adapters.LeadsListAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetLeadDetaiRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.CallLogResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadCallHistoryFragment extends BaseFragment {
    private static final String TAG = LeadCallHistoryFragment.class.getSimpleName();
    @BindView(R.id.cvBtnAdd)
    CardView cvBtnAdd;
    @BindView(R.id.callLogRV)
    RecyclerView callLogRV;
    @BindView(R.id.txvNoDataFound)
    TextView txvNoDataFound;

    private Context context;
    private static String leadId = "";
    public String title = "";
    private ArrayList<CallLogResponse> arrayList = new ArrayList<>();
    private MediaPlayer mediaPlayer;
    private ImageView ivBtnPlayPause;
    private File downloadFile, voiceNoteFile;
    private boolean voiceNoteRecordstarted = false;
    private CardView cvPlayDeleteOpt;
    private ImageView ivRecordingStatus, ivBtnPlay;
    private TextView txvVoiceNote, txvBtnStopStartVoiceRecord, txvBtnPlay, recordingTime;
    private MediaRecorder voiceNoteRecorder;
    private CountDownTimer countDownTimer;
    private long countUpmilliseconds = 0;

    public static Fragment getFragment(String leadsId) {
        leadId = leadsId;
        return new LeadCallHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmrnt_lead_call_log, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            initialise();
    }

    private void initialise() {
        context = getContext();
        callLogRV.setLayoutManager(new LinearLayoutManager(context));
        getAgentsCallLogAPI();

        cvBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddCallLogDialogue();
            }
        });

        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countUpmilliseconds = countUpmilliseconds + 1;

                int hours = (int) countUpmilliseconds / 3600;
                int remainder = (int) countUpmilliseconds - hours * 3600;
                int mins = remainder / 60;
                remainder = remainder - mins * 60;
                int secs = remainder;

                String text = String.format("%02d:%02d:%02d ", hours, mins, secs);
                if (recordingTime != null)
                    recordingTime.setText(text);
            }

            @Override
            public void onFinish() {
                countUpmilliseconds = 0;
                if (txvBtnStopStartVoiceRecord != null)
                    txvBtnStopStartVoiceRecord.setText(context.getResources().getString(R.string.record));
                voiceNoteRecordstarted = false;
                if (ivRecordingStatus != null)
//                    ivRecordingStatus.setImageDrawable(context.getDrawable(R.drawable.ic_mic));
                    ivRecordingStatus.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),R.drawable.ic_mic,null));

            }
        };
    }

    private void showAddCallLogDialogue() {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_call_log);
        final Window window = dialog.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;

        RadioGroup rgLeadSource = dialog.findViewById(R.id.rgLeadSource);
        EditText edtNote = dialog.findViewById(R.id.edtNote);
        cvPlayDeleteOpt = dialog.findViewById(R.id.cvPlayDeleteOpt);
        ivRecordingStatus = dialog.findViewById(R.id.ivRecordingStatus);
        txvVoiceNote = dialog.findViewById(R.id.txvVoiceNote);
        txvBtnPlay = dialog.findViewById(R.id.txvBtnPlay);
        ivBtnPlay = dialog.findViewById(R.id.ivBtnPlay);
        txvBtnStopStartVoiceRecord = dialog.findViewById(R.id.txvBtnStopStartVoiceRecord);
        recordingTime = dialog.findViewById(R.id.recordingTime);

        txvBtnStopStartVoiceRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!voiceNoteRecordstarted) {
                    Utility.ShowToastMessage(context, "Voice note recording Started!");
//                    ivRecordingStatus.setImageDrawable(context.getDrawable(R.drawable.ic_stop));
                    ivRecordingStatus.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_stop, null));
                    startRecord(SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER));
                } else {
                    Utility.ShowToastMessage(context, "Voice note Saved!");
                    stopRecord();
                }
            }
        });

        ((LinearLayout) dialog.findViewById(R.id.llBtnPlayPause)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (voiceNoteFile != null && mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                    txvBtnPlay.setText(R.string.play);
//                    ivBtnPlay.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                    ivBtnPlay.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));
                } else if (voiceNoteFile != null && !mediaPlayer.isPlaying()) {
                    playAudioFile();
                }
            }
        });

        ((LinearLayout) dialog.findViewById(R.id.llBtnDelete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voiceNoteFile = null;
                txvVoiceNote.setText("");
                recordingTime.setText("00:00:00");
                txvBtnPlay.setText(R.string.play);
//                ivBtnPlay.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                ivBtnPlay.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));

                cvPlayDeleteOpt.setVisibility(View.GONE);
            }
        });
        ((TextView) dialog.findViewById(R.id.txvButtonSave)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (voiceNoteFile == null && edtNote.getText().toString().isEmpty())
                    Utility.ShowToastMessage(context, "Add voice note or text note for entry");
                else {
                    Log.e(TAG, "lead_id: " + leadId);
                    Log.e(TAG, "customer_id: " + SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID));
                    Log.e(TAG, "user_email: " + SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
                    if (voiceNoteFile != null)
                        Log.e(TAG, "voice_record_url: " + voiceNoteFile.getPath());
                    Log.e(TAG, "source: " + ((RadioButton) rgLeadSource.getChildAt(rgLeadSource.indexOfChild(rgLeadSource.findViewById(rgLeadSource.getCheckedRadioButtonId())))).getText().toString());
                    Log.e(TAG, "notes: " + txvVoiceNote.getText().toString());
                    dialog.dismiss();
                }
            }
        });

        ((TextView) dialog.findViewById(R.id.txvButtonCancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voiceNoteFile = null;
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    private void startRecord(String fileName) {
        try {
            File sampleDir = new File(context.getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/VoiceNotes");
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String file_name = "VoiceNote" + "_" + fileName + "_" + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()) + "_";
            voiceNoteFile = File.createTempFile(file_name, ".mp3", sampleDir);
            voiceNoteRecorder = new MediaRecorder();
            voiceNoteRecorder.reset();

            voiceNoteRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

            voiceNoteRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

            voiceNoteRecorder.setOutputFile(voiceNoteFile.getAbsolutePath());

            voiceNoteRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            voiceNoteRecorder.setAudioSamplingRate(44100);
            voiceNoteRecorder.setAudioEncodingBitRate(196000);

            voiceNoteRecorder.prepare();
            voiceNoteRecorder.start();
            voiceNoteRecordstarted = true;
            Log.e(TAG, "startRecord: " + voiceNoteFile.getAbsolutePath());

            countDownTimer.start();
            mediaPlayer = new MediaPlayer();
            if (txvBtnStopStartVoiceRecord != null)
                txvBtnStopStartVoiceRecord.setText(context.getResources().getString(R.string.stop));
            cvPlayDeleteOpt.setVisibility(View.GONE);
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void stopRecord() {
        try {
            if (voiceNoteRecordstarted) {
                voiceNoteRecorder.stop();
                voiceNoteRecorder.reset();
                voiceNoteRecorder.release();
                countDownTimer.cancel();
                countDownTimer.onFinish();
                Log.e(TAG, "stop Voice Note Record: " + voiceNoteFile.getName() + " Size: " + (voiceNoteFile.getTotalSpace()) + " " + voiceNoteFile.getFreeSpace());

                if (txvVoiceNote != null)
                    txvVoiceNote.setText(voiceNoteFile.getName());
                cvPlayDeleteOpt.setVisibility(View.VISIBLE);
                if (ivRecordingStatus != null)
                    ivRecordingStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_mic));

            }
        } catch (Exception e) {
            Log.e(TAG, "stopRecord Exception : " + e.getMessage());
        }
    }

    private void playAudioFile() {
        Log.e(TAG, "playAudioFile: " + Uri.fromFile(voiceNoteFile));
        try {
            FileInputStream fis = new FileInputStream(voiceNoteFile);
            mediaPlayer.setDataSource(fis.getFD());
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build());
            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            mediaPlayer.prepare();

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer = new MediaPlayer();
                    if (ivBtnPlay != null)
//                        ivBtnPlay.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                        ivBtnPlay.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));

                    txvBtnPlay.setText(R.string.play);
                }
            });

            txvBtnPlay.setText(R.string.pause);
            if (ivBtnPlay != null)
//                ivBtnPlay.setImageDrawable(context.getDrawable(R.drawable.ic_pause));
                ivBtnPlay.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_pause, null));

        } catch (Exception e) {
            Utility.ShowToastMessage(context, e.getMessage());
            mediaPlayer = new MediaPlayer();
            txvBtnPlay.setText(R.string.play);
            if (ivBtnPlay != null)
//                ivBtnPlay.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                ivBtnPlay.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));

        }
    }

    private void getAgentsCallLogAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetLeadDetaiRequest object = new GetLeadDetaiRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setLead_id(leadId);
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getAgentsCallLogAPI(object).enqueue(new Callback<ArrayList<CallLogResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<CallLogResponse>> call, Response<ArrayList<CallLogResponse>> response) {
                    stopProgressHud(context);
                    if (response.body() != null && response.body().size() != 0) {
                        callLogRV.setVisibility(View.VISIBLE);
                        txvNoDataFound.setVisibility(View.GONE);
                        arrayList = response.body();
                        callLogRV.setAdapter(new LeadCallHistoryAdapter(context, arrayList, LeadCallHistoryFragment.this));
                    } else {
                        callLogRV.setVisibility(View.GONE);
                        txvNoDataFound.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<CallLogResponse>> call, Throwable t) {
                    stopProgressHud(context);

                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    public void downloadFile(String title, int position, String url) {
        this.title = title;
        try {
            File sampleDir = new File(context.getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/LeadCallRecordings");
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String file_name = "VoiceNote" + "_" + arrayList.get(position).getUser_name();
            downloadFile = File.createTempFile(file_name, ".mp3", sampleDir);
            Log.i("DownloadTask:- ", "ToExecuteUrl:-" + arrayList.get(position).getFile_url() + "clicked position: " + position);
            new DownloadTask(context, downloadFile, "Downloading").execute(arrayList.get(position).getFile_url());
        } catch (Exception e) {
            Log.e(LeadsListAdapter.class.getSimpleName(), "onClick: " + e.getMessage());
        }
    }

    private void playAudioUrlFile() {
        Log.e(TAG, "playAudioFile: " + Uri.fromFile(downloadFile));
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(context, Uri.fromFile(downloadFile));
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build());
            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            mediaPlayer.prepare();
            mediaPlayer.start();

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer = new MediaPlayer();
                    /*ivBtnPlayPause.setImageDrawable(context.getDrawable(R.drawable.ic_play));*/
                    ivBtnPlayPause.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));

                }
            });

            /* ivBtnPlayPause.setImageDrawable(context.getDrawable(R.drawable.ic_pause));*/
            ivBtnPlayPause.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_pause, null));
        } catch (Exception e) {
            Log.e(TAG, "playAudioFile: " + e.getMessage());
            mediaPlayer = new MediaPlayer();
//            ivBtnPlayPause.setImageDrawable(context.getDrawable(R.drawable.ic_play));
            ivBtnPlayPause.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));

        }
    }

    public void showDialog() {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_download_play);

        ImageView ivBtnClose = (ImageView) dialog.findViewById(R.id.ivBtnClose);
        ivBtnPlayPause = (ImageView) dialog.findViewById(R.id.ivBtnPlayPause);

        ((TextView) dialog.findViewById(R.id.txvTitle)).setText(title);

        ivBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ivBtnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer=null;
                    ivBtnPlayPause.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_play, null));
                } else {
                    playAudioUrlFile();
                }
            }
        });

        dialog.show();
        ViewGroup.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

    }

    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private ProgressDialog mPDialog;
        private Context mContext;
        private PowerManager.WakeLock mWakeLock;
        private File mTargetFile;

        public DownloadTask(Context context, File targetFile, String dialogMessage) {
            this.mContext = context;
            this.mTargetFile = targetFile;
            mPDialog = new ProgressDialog(context);
            mPDialog.setMessage(dialogMessage);
            mPDialog.setIndeterminate(true);
            mPDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mPDialog.setCancelable(false);
            // reference to instance to use inside listener
            final DownloadTask me = this;
            mPDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    me.cancel(true);
                }
            });
            Log.i("DownloadTask", "Constructor done");
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                Log.i("DownloadDIB:- ", sUrl[0]);
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }
                Log.i("DownloadTask", "Response " + connection.getResponseCode());

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();
                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(mTargetFile, false);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        Log.i("DownloadTask", "Cancelled");
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                Log.i(TAG, e.getMessage(), e);
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {

                }
                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
            mPDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mPDialog.setIndeterminate(false);
            mPDialog.setMax(100);
            mPDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i("DownloadTask", "Work Done! PostExecute");
            mWakeLock.release();
            mPDialog.dismiss();
            showDialog();
            if (result != null) {
                Toast.makeText(mContext, "Download error: " + result, Toast.LENGTH_LONG).show();
                Log.i("Download error: ", result);
            } else
                Toast.makeText(mContext, "File Downloaded", Toast.LENGTH_SHORT).show();
        }
    }
}
