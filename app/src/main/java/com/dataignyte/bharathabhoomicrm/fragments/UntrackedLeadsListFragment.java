package com.dataignyte.bharathabhoomicrm.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;
import com.dataignyte.bharathabhoomicrm.adapters.UntrackedLeadsListAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UntrackedLeadsListFragment extends BaseFragment {

    private static final String TAG = UntrackedLeadsListFragment.class.getSimpleName();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txvButtonBackToHome)
    TextView txvButtonBackToHome;
    @BindView(R.id.txvDeading)
    TextView txvDeading;
    @BindView(R.id.txvNoDataFound)
    TextView txvNoDataFound;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;

    private Context context;
    private UntrackedLeadsListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_list, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        txvButtonBackToHome.setVisibility(View.VISIBLE);
        txvDeading.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                getDashBoardAgentList();
                swipeLayout.setRefreshing(false);
            }
        });

        MainActivitySideDrawer.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (adapter!=null)
                    adapter.getFilter().filter(s.toString());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDashBoardAgentList();
    }

    @OnClick({R.id.txvButtonBackToHome})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonBackToHome:
                ((MainActivitySideDrawer)getActivity()).setFragments(new DashBoardFragment(), null, false);
                break;
        }
    }

        private void getDashBoardAgentList() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getUntrackedLeadsList(object).enqueue(new Callback<ArrayList<GetDashBoardResponse>>() {
                @Override
                public void onResponse(Call<ArrayList<GetDashBoardResponse>> call, Response<ArrayList<GetDashBoardResponse>> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body() != null && response.body().size() != 0) {
                            txvNoDataFound.setVisibility(View.GONE);
                            adapter = new UntrackedLeadsListAdapter(context, response.body());
                            recyclerView.setAdapter(adapter);
                        } else {
                            txvNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<GetDashBoardResponse>> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getDashBoardAgentList onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}
