package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.LeadInformationAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.EditFieldObject;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.EditLeadDataRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetLeadDetaiRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetLeadDetaiResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadInfoData;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UntrackedLeadDetailsFragment extends BaseFragment {

    private static final String TAG = UntrackedLeadDetailsFragment.class.getSimpleName();
    @BindView(R.id.txvButtonEdit)
    TextView txvButtonEdit;
    @BindView(R.id.leadInformationRV)
    RecyclerView leadInformationRV;
    @BindView(R.id.txvButtonSave)
    TextView txvButtonSave;

    private Context context;
    private LeadInformationAdapter leadInformationAdapter;
    private static String leadId = "";
    private ArrayList<LeadInfoData> leadInformationList = new ArrayList<>();

    public static Fragment getFragment(String leadsId) {
        leadId = leadsId;
        return new UntrackedLeadDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_deatils, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        leadInformationRV.setLayoutManager(new LinearLayoutManager(context));
        getLeadInfoDetails();
    }

    @OnClick({R.id.txvButtonEdit, R.id.txvButtonSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonSave:
                boolean callApi = true;
                for (int i = 0; i < leadInformationList.size(); i++) {
                    if (leadInformationList.get(i).isEditable())
                        if (leadInformationList.get(i).getKey().toLowerCase().equalsIgnoreCase("email") && !leadInformationList.get(i).getValue().isEmpty()) {
                            if (!Patterns.EMAIL_ADDRESS.matcher(leadInformationList.get(i).getValue()).matches()) {
                                Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_email_address));
                                callApi = false;
                                break;
                            }
                        } else if (leadInformationList.get(i).getKey().trim().equalsIgnoreCase("Projects")) {
                            if (leadInformationList.get(i).getValue().equalsIgnoreCase("Select Project:")) {
                                Utility.ShowToastMessage(context, "Please select project ");
                                callApi = false;
                                break;
                            }
                        } else if (leadInformationList.get(i).getKey().trim().equalsIgnoreCase("Lead_Source")) {
                            if (leadInformationList.get(i).getValue().equalsIgnoreCase("Select Lead Source:")) {
                                Utility.ShowToastMessage(context, "Please select lead source");
                                callApi = false;
                                break;
                            }
                        }
                }
                if (callApi)
                    callUpdateLeadDetailsAPI();
                break;
            case R.id.txvButtonEdit:
                txvButtonSave.setVisibility(View.VISIBLE);
                LeadInformationAdapter.setEditable = true;
                leadInformationAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void onValueChanged(int position, String value) {
        Log.e(TAG, "onValueChanged: "+ position+ "  "+value );
        leadInformationList.get(position).setValue(value);
    }

    private void getLeadInfoDetails() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetLeadDetaiRequest object = new GetLeadDetaiRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setLead_id(leadId);
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getUntrackedLeadInfoDetails(object).enqueue(new Callback<GetLeadDetaiResponse>() {
                @Override
                public void onResponse(Call<GetLeadDetaiResponse> call, Response<GetLeadDetaiResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        leadInformationList = response.body().getData();
                        leadInformationAdapter = new LeadInformationAdapter(context, UntrackedLeadDetailsFragment.this, leadInformationList);
                        leadInformationRV.setAdapter(leadInformationAdapter);
                    }
                }

                @Override
                public void onFailure(Call<GetLeadDetaiResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getLeadInfoDetails onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void callUpdateLeadDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            EditLeadDataRequest ob = new EditLeadDataRequest();

            ob.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            ob.setLead_id(leadId);
            ob.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            ob.setUser_role(SharedPref.getSharedPreferences(context, Constants.USER_ROLE));
            ArrayList<EditFieldObject> arrayList = new ArrayList<>();
            for (int i = 0; i < leadInformationList.size(); i++) {
                if (leadInformationList.get(i).isEditable())
                    arrayList.add(new EditFieldObject(leadInformationList.get(i).getKey(), leadInformationList.get(i).getValue()));
            }
            ob.setEditfield(arrayList);

            RetrofitClient.getAPIService().callUpdateUntrackedLeadDetailsAPI(ob).enqueue(new Callback<SuccessResponse>() {
                @Override
                public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body() != null && response.body().getSuccess() != null)
                            Utility.ShowToastMessage(context, response.body().getSuccess());
                        else
                            Utility.ShowToastMessage(context, response.body().getError());

                        Log.e(TAG, "callUpdateLeadDetailsAPI onResponse: " + new Gson().toJson(response.body()));
                        txvButtonSave.setVisibility(View.GONE);
                        LeadInformationAdapter.setEditable = false;
                        leadInformationAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<SuccessResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "callUploadLeadLogFileAPI onFailure: " + t.getMessage());
                    getActivity().finish();
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
        getActivity().finish();
    }
}
