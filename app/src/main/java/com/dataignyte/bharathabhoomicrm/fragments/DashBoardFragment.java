package com.dataignyte.bharathabhoomicrm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.MainActivitySideDrawer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashBoardFragment extends BaseFragment {

    private static MainActivitySideDrawer mainActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        mainActivity = ((MainActivitySideDrawer) getActivity());
        return view;
    }

    @OnClick({R.id.rrTabMyLeads, R.id.rrTabUnTrackedLeads, R.id.rrTabMyProfile, R.id.rrTabDocuments, R.id.rrTabSupport, R.id.rrTabAbout, R.id.rrTabLogout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rrTabMyLeads:
                mainActivity.txvToolbarTittle.setText(R.string.myLeads);
                mainActivity.setFragments(new LeadsListFragment(), null, false);
                break;
            case R.id.rrTabUnTrackedLeads:
                mainActivity.txvToolbarTittle.setText(R.string.un_tracked_leads);
                mainActivity.setFragments(new UntrackedLeadsListFragment(), null, false);
                break;
            case R.id.rrTabMyProfile:
                mainActivity.txvToolbarTittle.setText(R.string.profile);
                mainActivity.setFragments(new ProfileFragments(), null, false);
                break;
            case R.id.rrTabDocuments:
                mainActivity.txvToolbarTittle.setText(R.string.documents);
                mainActivity.setFragments(new DocumentsFragment(), null, false);
                break;
            case R.id.rrTabSupport:
                mainActivity.txvToolbarTittle.setText(R.string.support);
                mainActivity.setFragments(new SupportFragment(), null, false);
                break;
            case R.id.rrTabAbout:
                mainActivity.txvToolbarTittle.setText(R.string.about);
                mainActivity.setFragments(new AboutFragments(), null, false);
                break;
            case R.id.rrTabLogout:
                mainActivity.logoutDialogue();
                break;
        }
    }
}
