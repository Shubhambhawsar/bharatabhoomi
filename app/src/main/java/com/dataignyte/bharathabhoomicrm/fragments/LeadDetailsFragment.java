package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.LeadInformationAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.EditFieldObject;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.EditLeadDataRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetLeadDetaiRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetLeadDetaiResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadInfoData;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.StageObject;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;
import com.google.gson.Gson;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadDetailsFragment extends BaseFragment {

    private static final String TAG = LeadDetailsFragment.class.getSimpleName();
    @BindView(R.id.txvButtonEdit)
    TextView txvButtonEdit;
    @BindView(R.id.leadInformationRV)
    RecyclerView leadInformationRV;
    @BindView(R.id.txvButtonSave)
    TextView txvButtonSave;
    @BindView(R.id.llStage)
    LinearLayout llStage;
    @BindView(R.id.spnStage)
    Spinner spnStage;
    @BindView(R.id.spnSubStage)
    Spinner spnSubStage;

    private Context context;
    private LeadInformationAdapter leadInformationAdapter;
    private static String leadId = "";
    private String leadStage = "", leadSubStage = "";
    private ArrayList<LeadInfoData> leadInformationList = new ArrayList<>();
    private ArrayList<String> leadStageList = new ArrayList<>();
    private ArrayList<String> leadSubStageList = new ArrayList<>();
    private StageObject stageObject;

    public static Fragment getFragment(String leadsId) {
        leadId = leadsId;
        return new LeadDetailsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_deatils, container, false);
        ButterKnife.bind(this, view);
        initialise();
        return view;
    }

    private void initialise() {
        context = getContext();
        leadInformationRV.setLayoutManager(new LinearLayoutManager(context));

        leadStageList = Constants.LEAD_STAGE_ARRAY;
        setSpinner(leadStageList, spnStage);
        setSpnSelectListener();

        getLeadInfoDetails();
    }

    private void setSpnSelectListener() {
        spnStage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    leadStage = "";
                    leadSubStageList = new ArrayList<>();
                    setSpinner(leadSubStageList, spnSubStage);
                } else if (position ==1) {
                    leadStage = parent.getItemAtPosition(position).toString();
                    leadSubStageList = Constants.LEAD_SUB_STAGE_FOLLOWUP_STATUS_ARRAY;
                } else if (position ==2) {
                    leadStage = parent.getItemAtPosition(position).toString();
                    leadSubStageList = Constants.LEAD_SUB_STAGE_SITE_VISIT_ARRAY;
                } else if (position ==3) {
                    leadStage = parent.getItemAtPosition(position).toString();
                    leadSubStageList = Constants.LEAD_SUB_STAGE_NEGOTIATION_ARRAY;
                }
                leadSubStage = "";
                setSpinner(leadSubStageList, spnSubStage);

                Log.e(TAG, "onItemSelected: "+leadStage );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnSubStage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position !=0) {
                    leadSubStage = parent.getItemAtPosition(position).toString();
                }

                Log.e(TAG, "onItemSelected: "+leadSubStage );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinner(ArrayList<String> spinnerArray,Spinner spinner) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, spinnerArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @OnClick({ R.id.txvButtonEdit, R.id.txvButtonSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonSave:
                boolean callApi = true;
                for (int i = 0; i < leadInformationList.size(); i++) {
                    if (leadInformationList.get(i).isEditable())
                        if (leadInformationList.get(i).getKey().toLowerCase().equalsIgnoreCase("email") && !leadInformationList.get(i).getValue().isEmpty()) {
                            if (!Patterns.EMAIL_ADDRESS.matcher(leadInformationList.get(i).getValue()).matches()) {
                                Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_email_address));
                                callApi = false;
                            } else if (leadInformationList.get(i).getKey().trim().equalsIgnoreCase("Projects")) {
                                if (leadInformationList.get(i).getValue().equalsIgnoreCase("Select Project:")) {
                                    Utility.ShowToastMessage(context, getResources().getString(R.string.select_project));
                                    callApi = false;
                                    break;
                                }
                            } else if (leadInformationList.get(i).getKey().trim().equalsIgnoreCase("Lead_Source")) {
                                if (leadInformationList.get(i).getValue().equalsIgnoreCase("Select Lead Source:")) {
                                    Utility.ShowToastMessage(context, "Please select lead source");
                                    callApi = false;
                                    break;
                                }
                            }
                        }
                }
                if (callApi) {
                    if (leadStage.length() != 0 && !leadStage.equalsIgnoreCase("Select lead stage:") && leadSubStage.length() == 0) {
                        Utility.ShowToastMessage(context, "Select sub stage");
                    } else {
                        Log.e(TAG, "leadStage: "+leadStage);
                        Log.e(TAG, "leadSubStage: "+leadSubStage);
                        callUpdateLeadDetailsAPI();
                    }
                }
                break;
            case R.id.txvButtonEdit:
                spnStage.setEnabled(true);
                spnSubStage.setEnabled(true);
                txvButtonSave.setVisibility(View.VISIBLE);
                LeadInformationAdapter.setEditable = true;
                leadInformationAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void onValueChanged(int position, String value) {
        Log.e(TAG, "onValueChanged: "+ position+ "  "+value );
        leadInformationList.get(position).setValue(value);
    }

    private void callUpdateLeadDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            EditLeadDataRequest ob = new EditLeadDataRequest();

            ob.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            ob.setLead_id(leadId);
            ob.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            ob.setUser_role(SharedPref.getSharedPreferences(context, Constants.USER_ROLE));
            ArrayList<EditFieldObject> arrayList = new ArrayList<>();
            for (int i = 0; i < leadInformationList.size(); i++) {
                if (leadInformationList.get(i).isEditable())
                    arrayList.add(new EditFieldObject(leadInformationList.get(i).getKey(), leadInformationList.get(i).getValue()));
            }

            arrayList.add(new EditFieldObject("lead_stage", leadStage));
            arrayList.add(new EditFieldObject("lead_sub_stage", leadSubStage));
            ob.setEditfield(arrayList);

            RetrofitClient.getAPIService().callUpdateLeadDetailsAPI(ob).enqueue(new Callback<SuccessResponse>() {
                @Override
                public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200) {
                        if (response.body()!=null && response.body().getSuccess() != null)
                            Utility.ShowToastMessage(context, response.body().getSuccess());
                        else
                            Utility.ShowToastMessage(context, response.body().getError());

                        Log.e(TAG, "callUpdateLeadDetailsAPI onResponse: " + new Gson().toJson(response.body()));
                        txvButtonSave.setVisibility(View.GONE);
                        LeadInformationAdapter.setEditable = false;
                        leadInformationAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<SuccessResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "callUploadLeadLogFileAPI onFailure: " + t.getMessage());
                    getActivity().finish();
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
        getActivity().finish();
    }

    private void getLeadInfoDetails() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            GetLeadDetaiRequest object = new GetLeadDetaiRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setLead_id(leadId);
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getLeadInfoDetails(object).enqueue(new Callback<GetLeadDetaiResponse>() {
                @Override
                public void onResponse(Call<GetLeadDetaiResponse> call, Response<GetLeadDetaiResponse> response) {
                    if (response.code() == 200) {
                        if (response.body()!=null && response.body().isEditable())
                            txvButtonEdit.setVisibility(View.VISIBLE);
                        else
                            txvButtonEdit.setVisibility(View.INVISIBLE);

                        leadInformationList = response.body().getData();
                        leadInformationAdapter = new LeadInformationAdapter(context, LeadDetailsFragment.this, leadInformationList);
                        leadInformationRV.setAdapter(leadInformationAdapter);

                        stageObject = response.body().getStageObject();

                        if (stageObject!=null && stageObject.getLeadStage()!=null && stageObject.getLeadStage().length()!=0) {
                            if (stageObject.isStageVisible())
                                llStage.setVisibility(View.VISIBLE);
                            else
                                llStage.setVisibility(View.GONE);

                            if (stageObject.getLeadStage().toLowerCase().equalsIgnoreCase("followup status"))
                                spnStage.setSelection(1);
                            else if (stageObject.getLeadStage().toLowerCase().equalsIgnoreCase("site visit"))
                                spnStage.setSelection(2);
                            else if (stageObject.getLeadStage().toLowerCase().equalsIgnoreCase("negotiation"))
                                spnStage.setSelection(3);
                            leadStage = stageObject.getLeadStage();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (stageObject!=null && stageObject.getLeadSubStage()!=null && stageObject.getLeadSubStage().length()!=0) {
                                    leadSubStage = stageObject.getLeadSubStage();
                                    if (leadSubStageList.contains(leadSubStage)){
                                        spnSubStage.setSelection(leadSubStageList.indexOf(leadSubStage));
                                    }
                                }
                                stopProgressHud(context);
                            }
                        }, 3000);

                        spnStage.setEnabled(false);
                        spnSubStage.setEnabled(false);
                    } else
                        stopProgressHud(context);
                }

                @Override
                public void onFailure(Call<GetLeadDetaiResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, "getLeadInfoDetails onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}
