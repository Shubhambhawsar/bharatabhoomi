package com.dataignyte.bharathabhoomicrm.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreFragments extends BaseFragment {

    @BindView(R.id.layDetails)
    LinearLayout layDetails;
    @BindView(R.id.txvStatus)
    TextView txvStatus;
    @BindView(R.id.txvCallType)
    TextView txvCallType;
    @BindView(R.id.txvEndTime)
    TextView txvEndTime;
    @BindView(R.id.txvCallStartTime)
    TextView txvCallStartTime;
    @BindView(R.id.txvNumber)
    TextView txvNumber;
    @BindView(R.id.txvLastCallDuration)
    TextView txvLastCallDuration;
    @BindView(R.id.layFileDetails)
    LinearLayout layFileDetails;
    @BindView(R.id.txvFileName)
    TextView txvFileName;
    @BindView(R.id.txvFilePath)
    TextView txvFilePath;
    @BindView(R.id.txvFileSize)
    TextView txvFileSize;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        ButterKnife.bind(this, view);
        context = getContext();
        checkeLastCallValuesToShow();
        return view;
    }

    private void checkeLastCallValuesToShow() {
        if (SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER) != null && SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER).length() != 0)
            layDetails.setVisibility(View.VISIBLE);
        else
            layDetails.setVisibility(View.GONE);

        txvNumber.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER)));
        txvCallStartTime.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.START_TIME)));
        txvEndTime.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.END_TIME)));
        txvCallType.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.CALL_TYPE)));
        txvLastCallDuration.setText(Utility.findDifferenceInTime(SharedPref.getSharedPreferences(context, Constants.START_TIME), SharedPref.getSharedPreferences(context, Constants.END_TIME)));
        if (SharedPref.getSharedPreferences(context, Constants.FILE_NAME)!=null && SharedPref.getSharedPreferences(context, Constants.FILE_NAME).length()!=0)
            layFileDetails.setVisibility(View.VISIBLE);
        else
            layFileDetails.setVisibility(View.GONE);

        txvFileName.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.FILE_NAME)));
        txvFilePath.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.FILE_PATH)));
        txvFileSize.setText(Utility.geNonNullString(SharedPref.getSharedPreferences(context, Constants.FILE_SIZE)));
    }
}
