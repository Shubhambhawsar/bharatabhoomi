package com.dataignyte.bharathabhoomicrm.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.LoginRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LoginResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.txvButtonLogin)
    TextView txvButtonLogin;

    private Context context;
    private String firebaseToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialise();
    }

    private void initialise() {
        context = this;
        getFirebaseToken();
    }

    private void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                firebaseToken =  instanceIdResult.getToken();
            }
        });
    }

    @OnClick({R.id.txvButtonLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonLogin:
                if (edtEmail.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, R.string.enter_your_email);
                else if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches())
                    Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_email_address));
                else if (edtPassword.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, R.string.enter_your_password);
                else
                    callLoginApi();
                break;
        }
    }

    private void callLoginApi() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            LoginRequest object = new LoginRequest();
            object.setUseremail(edtEmail.getText().toString().trim());
            object.setPassword(edtPassword.getText().toString().trim());
            object.setAndroid_version("" + android.os.Build.VERSION.RELEASE);
            object.setDevice_id(firebaseToken);
            object.setMobile_version(""+android.os.Build.VERSION.SDK_INT);
            RetrofitClient.getAPIService().callLoginAPI(object).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {
                        if (response.body().getStatus_id() == 1) {
                            Log.e(TAG, "onResponse: " + new Gson().toJson(response.body()));
                            SharedPref.setSharedPreference(context, Constants.CUSTOMER_ID, String.valueOf(response.body().getCustomer_id()));
                            SharedPref.setSharedPreference(context, Constants.USER_EMAIL, response.body().getUser_email());
                            SharedPref.setSharedPreference(context, Constants.USER_NAME, response.body().getUser_name());
                            SharedPref.setSharedPreference(context, Constants.USER_ROLE, response.body().getUser_role());
                            SharedPref.setSharedPreference(context, Constants.STATUS_ID, String.valueOf(response.body().getStatus_id()));
                            startActivity(new Intent(context, MainActivitySideDrawer.class));
                            finish();
                        } else {
                            Utility.ShowToastMessage(context, getString(R.string.enter_valid_credentials));
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "callLoginAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}