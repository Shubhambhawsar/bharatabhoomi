package com.dataignyte.bharathabhoomicrm.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.CallLogAdapter;
import com.dataignyte.bharathabhoomicrm.fragments.LeadsListFragment;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.LeadNameRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.CallLogObject;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadNameResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.FOREGROUND_SERVICE;
import static android.Manifest.permission.MODIFY_AUDIO_SETTINGS;
import static android.Manifest.permission.PROCESS_OUTGOING_CALLS;
import static android.Manifest.permission.READ_CALL_LOG;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.STORAGE;

public class CallLogActivity extends BaseActivity {

    private static final String TAG = CallLogActivity.class.getSimpleName();
    @BindView(R.id.rvCallHistory)
    RecyclerView rvCallHistory;
    @BindView(R.id.edtSearch)
    EditText edtSearch;
    private Context context;
    private ArrayList<CallLogObject> callLogObjectArrayList = new ArrayList<>();
    private CallLogAdapter callLogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);
        ButterKnife.bind(this);
        initilaise();
    }

    private void initilaise() {
        context = this;
        rvCallHistory.setLayoutManager(new LinearLayoutManager(context));
        if (!Utility.checkRuntimePermission(this, READ_PHONE_STATE, PROCESS_OUTGOING_CALLS, CALL_PHONE, READ_CALL_LOG, FOREGROUND_SERVICE, RECORD_AUDIO, MODIFY_AUDIO_SETTINGS, STORAGE, WRITE_EXTERNAL_STORAGE, READ_CONTACTS)) {
            ActivityCompat.requestPermissions(this, new String[]{READ_PHONE_STATE, PROCESS_OUTGOING_CALLS, CALL_PHONE, READ_CALL_LOG, FOREGROUND_SERVICE, RECORD_AUDIO, MODIFY_AUDIO_SETTINGS, STORAGE, WRITE_EXTERNAL_STORAGE, READ_CONTACTS}, 1101);
        } else {
            getCallLog();
        }

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (callLogAdapter!=null)
                    callLogAdapter.getFilter().filter(s.toString());
            }
        });
    }

    private void getCallLog() {
        startProgressHud();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCallDetails();
            }
        }, 1101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCallLog();
            } else {
                ActivityCompat.requestPermissions(this, permissions, 1101);
                Toast.makeText(this, "Permissions Needed", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getCallDetails() {
        Log.e(LeadsListFragment.class.getSimpleName(), "Agil value --- ");
        StringBuffer sb = new StringBuffer();
//        Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
        Cursor managedCursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int cachedNameIndex = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number); // mobile number
            String callType = managedCursor.getString(type); // call type
            String callDate = managedCursor.getString(date); // call date
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String cachedName = managedCursor.getString(cachedNameIndex);
            if (TextUtils.isEmpty(cachedName)) {
                cachedName = queryPhone(String.valueOf(number));
            }
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = Constants.CALL_TYPE_OUT_GOING;
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = Constants.CALL_TYPE_INCOMING;
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = Constants.CALL_TYPE_MISSED_CALL;
                    break;
            }
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");

            CallLogObject callObject = new CallLogObject();
            callObject.setPhoneNumber(phNumber);
            callObject.setPhoneNumber(phNumber);
            callObject.setCall_type(dir);
            callObject.setDateTime(callDayTime);
            callObject.setStart_time(callDayTime.toString());
            callObject.setName(cachedName);
            callObject.setCall_duration(callDuration);
            callLogObjectArrayList.add(callObject);
        }
        managedCursor.close();
        callLogObjectArrayList.sort(Comparator.comparing(o -> ((CallLogObject) o).getDateTime()).reversed());
        callLogAdapter = new CallLogAdapter(context, callLogObjectArrayList, this);
        rvCallHistory.setAdapter(callLogAdapter);
        stopProgressHud();
    }

    private String queryPhone(String number) {
        String name = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Cursor cursor = getContentResolver().query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor.moveToFirst()) {
            name = cursor.getString(0);
        }
        cursor.close();
        return name;
    }

    @OnClick(R.id.ivButtonDrawer)
    public void onViewClicked() {
        finish();
    }

    public void getLeadNameAPI(String name, String number, String call_duration, String date, String callType) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            LeadNameRequest ob = new LeadNameRequest();
            ob.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            ob.setPhone(number);
            RetrofitClient.getAPIService().getLeadNameAPI(ob).enqueue(new Callback<LeadNameResponse>() {
                @Override
                public void onResponse(Call<LeadNameResponse> call, Response<LeadNameResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {
                        SharedPref.setSharedPreference(context, Constants.SHOW_ADD_LEAD_DETAILS_FORM, String.valueOf(response.body().showForm()));
                        if (response.body().getName() != null) {
                            startActivity(new Intent(context, SaveCalLogDetailsActivity.class)
                                    .putExtra(Constants.LEAD_NAME, response.body().getName())
                                    .putExtra(Constants.LEAD_NUMBER, number)
                                    .putExtra(Constants.CALL_DURATION, call_duration)
                                    .putExtra(Constants.CALL_TYPE, callType)
                                    .putExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM, "false")
                                    .putExtra(Constants.LEADS_PROJECT, response.body().getProjects())
                                    .putExtra(Constants.CALL_DATE, date));
                            finish();
                        } else {
                            startActivity(new Intent(context, SaveCalLogDetailsActivity.class)
                                    .putExtra(Constants.LEAD_NAME, name)
                                    .putExtra(Constants.LEAD_NUMBER, number)
                                    .putExtra(Constants.CALL_DURATION, call_duration)
                                    .putExtra(Constants.CALL_TYPE, callType)
                                    .putExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM, Constants.TRUE)
                                    .putExtra(Constants.LEADS_PROJECT, "")
                                    .putExtra(Constants.CALL_DATE, date));
                            finish();
                        }
                    } else {
                        startActivity(new Intent(context, AddLeadActivity.class)
                                .putExtra(Constants.LEAD_NAME, name)
                                .putExtra(Constants.LEAD_NUMBER, number)
                                .putExtra(Constants.CALL_DURATION, call_duration)
                                .putExtra(Constants.CALL_TYPE, callType)
                                .putExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM, Constants.TRUE)
                                .putExtra(Constants.LEADS_PROJECT, "")
                                .putExtra(Constants.CALL_DATE, date));
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<LeadNameResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "getLeadNameAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

}