package com.dataignyte.bharathabhoomicrm.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.FileUploadResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveCalLogDetailsActivity extends BaseActivity {

    private static final String TAG = SaveCalLogDetailsActivity.class.getSimpleName();
    @BindView(R.id.ivButtonBack)
    ImageView ivButtonBack;
    @BindView(R.id.txvNumber)
    TextView txvNumber;
    @BindView(R.id.txvLeadName)
    TextView txvLeadNumber;
    @BindView(R.id.txvLastCallDuration)
    TextView txvLastCallDuration;
    @BindView(R.id.edtNote)
    EditText edtNote;
    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtLastName)
    EditText edtLastName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPlace)
    EditText edtPlace;
    @BindView(R.id.edtCity)
    EditText edtCity;
    @BindView(R.id.spnProjects)
    Spinner spnProjects;
    @BindView(R.id.edtScores)
    EditText edtScores;
    @BindView(R.id.edtOccupation)
    EditText edtOccupation;
    @BindView(R.id.spnSource)
    Spinner spnSource;
    @BindView(R.id.edtRemarks)
    EditText edtRemarks;
    @BindView(R.id.cvLeadInformation)
    CardView cvLeadInformation;

    @BindView(R.id.txvButtonSave)
    TextView txvButtonSave;
    @BindView(R.id.txvVoiceNote)
    TextView txvVoiceNote;
    @BindView(R.id.txvLeadProject)
    TextView txvLeadProject;
    @BindView(R.id.txvBtnPlay)
    TextView txvBtnPlay;
    @BindView(R.id.cvPlayDeleteOpt)
    CardView cvPlayDeleteOpt;

    MediaPlayer mediaPlayer = new MediaPlayer();
    private boolean showingForm = true ;
    public static boolean activityFlag = false;
    public static Context context;
    private MediaRecorder voiceNoteRecorder;
    private boolean voiceNoteRecordstarted = false;
    private CountDownTimer countDownTimer;
    private File voiceNoteFile;
    private long countUpmilliseconds = 0;

    String[] leadSourceList = {"Select Lead Source:"
            , "SSTV"
            , "Banner"
            , "Broucher distribution"
            , "Pomplet distribution"
            , "Advertisement in Some media"
            , "Website"
            , "facebook"
            , "W/A"
            , "Relatives"
            , "Existing client"
            , " Client manager own effort",},
            projectsList = {"Select Project:"
                    , "BBT"
                    , "SSC"
                    , "BBT/SSC"
                    , "Kuberapuram"
                    , "Others" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_call_log_details);
        ButterKnife.bind(this);

        initialise();
    }

    private void initialise() {
        context = this;
        activityFlag = true;

        if (getIntent().getStringExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM).equalsIgnoreCase(Constants.TRUE)) {
            showingForm = true;
            cvLeadInformation.setVisibility(View.VISIBLE);
        } else {
            showingForm = false;
            cvLeadInformation.setVisibility(View.GONE);
        }

        txvNumber.setText(getIntent().getStringExtra(Constants.LEAD_NUMBER));
        if (getIntent().getStringExtra(Constants.LEAD_NAME) != null && !getIntent().getStringExtra(Constants.LEAD_NAME).isEmpty())
            txvLeadNumber.setText(getIntent().getStringExtra(Constants.LEAD_NAME));

        if (getIntent().getStringExtra(Constants.LEADS_PROJECT) != null && !getIntent().getStringExtra(Constants.LEADS_PROJECT).isEmpty())
            txvLeadProject.setText(getIntent().getStringExtra(Constants.LEADS_PROJECT));

        txvLastCallDuration.setText(getIntent().getStringExtra(Constants.CALL_DURATION));

        ArrayAdapter spnProjectsAdapter = new ArrayAdapter(context, R.layout.spinner_textview, R.id.txvValue, projectsList);
        spnProjectsAdapter.setDropDownViewResource(R.layout.spinner_textview);
        spnProjects.setAdapter(spnProjectsAdapter);

        ArrayAdapter spnLeadSourceAdapter = new ArrayAdapter(context, R.layout.spinner_textview, R.id.txvValue, leadSourceList);
        spnLeadSourceAdapter.setDropDownViewResource(R.layout.spinner_textview);
        spnSource.setAdapter(spnLeadSourceAdapter);

        countDownTimer = new CountDownTimer(Long.MAX_VALUE, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countUpmilliseconds = countUpmilliseconds + 1;

                int hours = (int) countUpmilliseconds / 3600;
                int remainder = (int) countUpmilliseconds - hours * 3600;
                int mins = remainder / 60;
                remainder = remainder - mins * 60;
                int secs = remainder;

                String text = String.format("%02d:%02d:%02d ", hours, mins, secs);
                ((TextView) findViewById(R.id.recordingTime)).setText(text);
            }

            @Override
            public void onFinish() {
                countUpmilliseconds = 0;
                ((TextView) findViewById(R.id.txvBtnStopStartVoiceRecord)).setText(context.getResources().getString(R.string.record));
                voiceNoteRecordstarted = false;
                ((ImageView) findViewById(R.id.ivRecordingStatus)).setImageDrawable(getResources().getDrawable(R.drawable.ic_mic));
            }
        };
    }

    @OnClick({R.id.ivButtonBack, R.id.txvButtonCancel, R.id.txvButtonSave, R.id.ivRecordingStatus, R.id.txvBtnStopStartVoiceRecord,  R.id.llBtnPlayPause, R.id.llBtnDelete})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.ivRecordingStatus:
            case R.id.txvBtnStopStartVoiceRecord:
                if (!voiceNoteRecordstarted) {
                    Utility.ShowToastMessage(context, "Recording Started!");
                    ((ImageView) findViewById(R.id.ivRecordingStatus)).setImageDrawable(getResources().getDrawable(R.drawable.ic_stop));
                    startRecord(getIntent().getStringExtra(Constants.LEAD_NUMBER));
                } else {
                    Utility.ShowToastMessage(context, "Recording Saved!");
                    stopRecord();
                }
                break;

            case R.id.llBtnPlayPause:
                if (voiceNoteFile!=null && mediaPlayer!=null && mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                    txvBtnPlay.setText(R.string.play);
                    ((ImageView) findViewById(R.id.ivBtnPlay)).setImageDrawable(context.getDrawable(R.drawable.ic_play));
                } else {
                    playAudioFile();
                }
                break;

            case R.id.llBtnDelete:
                new AlertDialog.Builder(context)
                        .setMessage(R.string.error_delete_voice_note)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                voiceNoteFile = null;
                                ((TextView) findViewById(R.id.recordingTime)).setText("00:00:00");
                                txvBtnPlay.setText(R.string.play);
                                ((ImageView) findViewById(R.id.ivBtnPlay)).setImageDrawable(context.getDrawable(R.drawable.ic_play));
                                cvPlayDeleteOpt.setVisibility(View.GONE);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
                break;

            case R.id.txvButtonCancel:
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.txvButtonSave:
                if (voiceNoteRecordstarted)
                    Utility.ShowToastMessage(context, getResources().getString(R.string.stop_voice_note_recording));
                else if (!edtEmail.getText().toString().trim().isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches())
                    Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_email_address));
                else if (getIntent().getStringExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM).equalsIgnoreCase(Constants.TRUE)) {
                    if (edtLastName.getText().toString().trim().isEmpty())
                        edtLastName.setError(getString(R.string.enter_last_name));
                    else if (spnProjects.getSelectedItemPosition() == 0)
                        Utility.ShowToastMessage(context, getResources().getString(R.string.select_project));
                    else if (spnSource.getSelectedItemPosition() == 0)
                        Utility.ShowToastMessage(context, getResources().getString(R.string.select_lead_source));
                    else if (voiceNoteRecordstarted)
                        Utility.ShowToastMessage(context, getResources().getString(R.string.stop_voice_note_recording));
                    else
                        callUploadFromLeadCallLogAPI(txvNumber.getText().toString().trim(), txvLastCallDuration.getText().toString().trim(), edtNote.getText().toString().trim());
                } else
                    callUploadFromLeadCallLogAPI(txvNumber.getText().toString().trim(), txvLastCallDuration.getText().toString().trim(), edtNote.getText().toString().trim());
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void callUploadFromLeadCallLogAPI(String number, String call_duration, String note) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart(Constants.CUSTOMER_ID, SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID));
            builder.addFormDataPart(Constants.USER_EMAIL, SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            builder.addFormDataPart(Constants.CALLED_TO, number);
            builder.addFormDataPart(Constants.CALL_DURATION, call_duration);
            builder.addFormDataPart(Constants.NOTES, note);
            builder.addFormDataPart(Constants.LOG_TYPE, Constants.LOG_TYPE_CALL_LOG);

            if (voiceNoteFile != null) {
                builder.addFormDataPart(Constants.VOICE_RECORD_URL, voiceNoteFile.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), voiceNoteFile));
            }
            builder.addFormDataPart(Constants.FIRST_NAME, edtFirstName.getText().toString().trim());
            builder.addFormDataPart(Constants.LAST_NAME, edtLastName.getText().toString().trim());
            builder.addFormDataPart(Constants.FULL_NAME, edtLastName.getText().toString().trim());
            builder.addFormDataPart(Constants.EMAIL, edtEmail.getText().toString().trim());
            builder.addFormDataPart(Constants.PLACE, edtPlace.getText().toString().trim());
            builder.addFormDataPart(Constants.CITY, edtCity.getText().toString().trim());

            if(getIntent().getStringExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM).equalsIgnoreCase(Constants.TRUE))
                builder.addFormDataPart(Constants.PROJECTS, getSpinnerSelcetedItem(spnProjects));
            else
                builder.addFormDataPart(Constants.PROJECTS, txvLeadProject.getText().toString().trim());

            builder.addFormDataPart(Constants.REMARKS, edtRemarks.getText().toString().trim());
            builder.addFormDataPart(Constants.SCORES, edtScores.getText().toString().trim());
            builder.addFormDataPart(Constants.OCCUPATION, edtOccupation.getText().toString().trim());
            builder.addFormDataPart(Constants.LEAD_SOURCE, getSpinnerSelcetedItem(spnSource));
            builder.addFormDataPart(Constants.LEAD_STATUS, "");
            builder.addFormDataPart(Constants.CALL_TYPE, getIntent().getStringExtra(Constants.CALL_TYPE));

            if(getIntent().getStringExtra(Constants.SHOW_ADD_LEAD_DETAILS_FORM).equalsIgnoreCase(Constants.TRUE))
                builder.addFormDataPart(Constants.FORM, "1");
            else
                builder.addFormDataPart(Constants.FORM, "0");

            MultipartBody requestBody = builder.build();

            RetrofitClient.getAPIService().callUploadFromLeadCallLogAPI(requestBody).enqueue(new Callback<FileUploadResponse>() {
                @Override
                public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {
                        if (response.body().getSuccess() != null) {
                            Utility.ShowToastMessage(context, response.body().getSuccess());

                            startActivity(new Intent(context, MainActivitySideDrawer.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finishAffinity();
                        } else
                            Utility.ShowToastMessage(context, response.body().getError());
                    }
                }

                @Override
                public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                    Utility.ShowToastMessage(context, "Network problem! try again....");
                    stopProgressHud();
                    Log.e(TAG, "callUploadLeadLogFileAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityFlag = false;
        stopProgressHud();
    }

    private String getSpinnerSelcetedItem(Spinner spinner) {
        if (spinner.getSelectedItemPosition() == 0)
            return "";
        else
            return (String) spinner.getSelectedItem();
    }

    private void startRecord(String fileName) {
        try {
            File sampleDir = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/VoiceNotes");
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String file_name = "VoiceNote" + "_" + fileName + "_" + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()) + "_";
            voiceNoteFile = File.createTempFile(file_name, ".mp3", sampleDir);
            voiceNoteFile.setReadable(true, false);
            voiceNoteRecorder = new MediaRecorder();
            voiceNoteRecorder.reset();

            voiceNoteRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

            voiceNoteRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

            voiceNoteRecorder.setOutputFile(voiceNoteFile.getAbsolutePath());

            voiceNoteRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            voiceNoteRecorder.setAudioSamplingRate(44100);
            voiceNoteRecorder.setAudioEncodingBitRate(196000);

            voiceNoteRecorder.prepare();
            voiceNoteRecorder.start();
            voiceNoteRecordstarted = true;

            countDownTimer.start();
            mediaPlayer = new MediaPlayer();

            ((TextView) findViewById(R.id.txvBtnStopStartVoiceRecord)).setText(context.getResources().getString(R.string.stop));
            cvPlayDeleteOpt.setVisibility(View.GONE);
        } catch (IOException e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void stopRecord() {
        try {
            if (voiceNoteRecordstarted) {
                voiceNoteRecorder.stop();
                voiceNoteRecorder.reset();
                voiceNoteRecorder.release();
                countDownTimer.cancel();
                countDownTimer.onFinish();
                Log.e(TAG, "stop Voice Note Record: " + voiceNoteFile.getName() + " Size: " + (voiceNoteFile.getTotalSpace()) + " " + voiceNoteFile.getFreeSpace());
                txvVoiceNote.setText(voiceNoteFile.getName());
                cvPlayDeleteOpt.setVisibility(View.VISIBLE);
                ((ImageView) findViewById(R.id.ivRecordingStatus)).setImageDrawable(getResources().getDrawable(R.drawable.ic_mic));
            }
        } catch (Exception e) {
            Log.e(TAG, "stopRecord Exception : " + e.getMessage());
        }
    }

    private void playAudioFile() {
        Log.e(TAG, "playAudioFile: "+ Uri.fromFile(voiceNoteFile));
        try {
            FileInputStream fis = new FileInputStream(voiceNoteFile);
            mediaPlayer.setDataSource(fis.getFD());
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mediaPlayer.setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build());
            } else {
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                    ((ImageView) findViewById(R.id.ivBtnPlay)).setImageDrawable(context.getDrawable(R.drawable.ic_play));
                    txvBtnPlay.setText(R.string.play);
                }
            });

            txvBtnPlay.setText(R.string.pause);
            ((ImageView) findViewById(R.id.ivBtnPlay)).setImageDrawable(context.getDrawable(R.drawable.ic_pause));
        } catch (Exception e) {
            Utility.ShowToastMessage(context, ""+e.getMessage());
            Log.e(TAG, "playAudioFile: "+e.getClass().getName() );
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = new MediaPlayer();
            }
            txvBtnPlay.setText(R.string.play);
            ((ImageView) findViewById(R.id.ivBtnPlay)).setImageDrawable(context.getDrawable(R.drawable.ic_play));
        }
    }
}