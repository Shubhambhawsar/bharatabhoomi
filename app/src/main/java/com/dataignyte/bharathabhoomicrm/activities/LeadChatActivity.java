package com.dataignyte.bharathabhoomicrm.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.LeadChatMessagAdapter;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.LeadAgentConversationRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.SendMessageRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.ConversationMessage;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadAgentConversationResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadChatActivity extends BaseActivity {

    private static final String TAG = LeadChatActivity.class.getSimpleName();
    @BindView(R.id.txvTitle)
    TextView txvTitle;
    @BindView(R.id.chatListRV)
    RecyclerView chatListRV;
    @BindView(R.id.edtMessage)
    EditText edtMessage;
    private Context context;
    private String leadId;
    private LeadChatMessagAdapter adapter;
    private ArrayList<ConversationMessage> conversationMessages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_chat);
        ButterKnife.bind(this);
        initialise();
    }

    private void initialise() {
        context = this;
        leadId = getIntent().getStringExtra(Constants._ID);
        txvTitle.setText(getIntent().getStringExtra(Constants.LEAD_NAME));
        chatListRV.setLayoutManager(new LinearLayoutManager(context));
        adapter = new LeadChatMessagAdapter(context, conversationMessages, true);
        chatListRV.setAdapter(adapter);
        getLeadAgentConversationMessagesAPI();
    }

    @OnClick({R.id.ivButtonBack, R.id.ivButtonRefresh, R.id.ivButtonSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.ivButtonRefresh:
                getLeadAgentConversationMessagesAPI();
                break;
            case R.id.ivButtonSend:
                if (edtMessage.getText().toString().trim().length()!=0)
                    callSendMessagesAPI(edtMessage.getText().toString().trim());
                break;
        }
    }

    private void getLeadAgentConversationMessagesAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            LeadAgentConversationRequest object = new LeadAgentConversationRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            object.setLead_id(leadId);
            RetrofitClient.getAPIService().getLeadAgentConversationMessagesAPI(object).enqueue(new Callback<LeadAgentConversationResponse>() {
                @Override
                public void onResponse(Call<LeadAgentConversationResponse> call, Response<LeadAgentConversationResponse> response) {
                    stopProgressHud();
                    if (response!=null && response.body().getData()!=null && response.body().getData().size()!=0) {
                        conversationMessages = response.body().getData();
                        adapter = new LeadChatMessagAdapter(context, conversationMessages, true);
                        chatListRV.setAdapter(adapter);
                        chatListRV.scrollToPosition((conversationMessages.size()-1));
                    }
                }

                @Override
                public void onFailure(Call<LeadAgentConversationResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "getLeadAgentConversationMessagesAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void callSendMessagesAPI(String message) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            SendMessageRequest object = new SendMessageRequest();
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            object.setLead_id(leadId);
            object.setMessage(message);
            RetrofitClient.getAPIService().callSendMessagesAPI(object).enqueue(new Callback<SuccessResponse>() {
                @Override
                public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                    stopProgressHud();
                    if (response.body()!=null && response.body().getSuccess()!=null) {
                        ConversationMessage object = new ConversationMessage();
                        object.setText(message);
                        object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
                        object.setUser_role(SharedPref.getSharedPreferences(context, Constants.USER_ROLE));
                        object.setUsername(SharedPref.getSharedPreferences(context, Constants.USER_NAME));
                        object.setTime(new SimpleDateFormat("dd-MMM-yyyy hh:mm: a").format(new Date()));
                        conversationMessages.add(object);
                        adapter.updateArrayList(conversationMessages);
                        if (conversationMessages.size()!=1)
                            chatListRV.scrollToPosition((conversationMessages.size()-1));
                        edtMessage.setText("");
                    }
                }

                @Override
                public void onFailure(Call<SuccessResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "callSendMessagesAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}