package com.dataignyte.bharathabhoomicrm.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.dataignyte.bharathabhoomicrm.services.InvokeService;
import com.dataignyte.bharathabhoomicrm.services.PhoneCallsReciever;
import java.io.File;

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = BaseActivity.class.getSimpleName();
    public static PhoneCallsReciever outgoingReceiver;
    public static File audiofile;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        outgoingReceiver = new PhoneCallsReciever();
    }

    public void registerCallReceiver() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
        LocalBroadcastManager.getInstance(this).registerReceiver(outgoingReceiver, intentFilter);
        startService(new Intent(this, InvokeService.class));
    }

    public void unRegisterCallReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(outgoingReceiver);
        stopService(new Intent(this, InvokeService.class));
    }

    public void startProgressHud() {
        if (progressDialog==null || !progressDialog.isShowing()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Please wait loading....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public void stopProgressHud() {
        if(progressDialog!=null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopProgressHud();
    }
}
