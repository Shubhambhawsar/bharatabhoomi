package com.dataignyte.bharathabhoomicrm.activities;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.*;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.app.NotificationCompat;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.LeadNameRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadNameResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallerInfoDialogActivity extends BaseActivity {
    private static final String TAG = CallerInfoDialogActivity.class.getSimpleName();
    private Context context;
    private Dialog dialog;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        getLeadNameAPI();
    }

    public void showDialogue(LeadNameResponse response, String leadName, String lastCallTime) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_caller_info);
        final Window window = dialog.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setDimAmount(0.5f);
        window.setAttributes(wlp);

        ImageView ivLeadImg = dialog.findViewById(R.id.ivLeadImg);
        if (leadName!=null) {
            ((TextView) dialog.findViewById(R.id.txvLeadName)).setText(response.getSL_NO() + " - "+ leadName);
            ((TextView) dialog.findViewById(R.id.txvProjectName)).setText("Project: "+ response.getProjects());
            ((TextView) dialog.findViewById(R.id.txvAssociateName)).setText("Associate: "+ response.getAssociate_name());
            ((TextView) dialog.findViewById(R.id.txvPlace)).setText("Place: "+ response.getPlace());
            SharedPref.setSharedPreference(context, Constants.LEAD_NAME, response.getSL_NO() + " - "+ leadName);
            SharedPref.setSharedPreference(context, Constants.LEADS_PROJECT, response.getProjects());
        } else {
            SharedPref.setSharedPreference(context, Constants.LEAD_NAME, "");
            SharedPref.setSharedPreference(context, Constants.LEADS_PROJECT, "");
        }
        if(lastCallTime!=null)
            ((TextView) dialog.findViewById(R.id.txvLastCallInfo)).setText(context.getResources().getString(R.string.last_call)+ " "+lastCallTime);

        ((TextView) dialog.findViewById(R.id.txvLeadNumber)).setText("Number: "+SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER));
        ImageView ivButtonClose = dialog.findViewById(R.id.ivButtonClose);

        ivButtonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.dismiss();
                finish();
            }
        });
        dialog.setCancelable(false);
        dialog.show();

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                    finish();
                } catch (Exception e) {
                    finish();
                }
            }
        }, 16411);
    }

    public void showNotification(LeadNameResponse response, String leadName, String lastCallTime) {
        String title = "", message = "", bigMessage =  "",  lastCallInfo = "";

        if (leadName != null) {
            title = response.getSL_NO() + " - "+ leadName;
            message = "Project: "+ response.getProjects()
                    + "\n" + "Associate: "+ response.getAssociate_name();
            bigMessage = "Project: "+ response.getProjects()
                    + "\n" + "Associate: "+ response.getAssociate_name()
                    + "\n" + "Place: "+ response.getPlace()
                    + "\n" + context.getResources().getString(R.string.last_call)+ " "+lastCallTime;
            if(lastCallTime!=null)
                    lastCallInfo = context.getResources().getString(R.string.last_call)+ " "+lastCallTime;
        } else  {
            title = "Number: "+SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER);
            message = "";
            bigMessage = "";
            lastCallInfo = "";
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = context.getString(R.string.app_name);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Caller Info Notifications", NotificationManager.IMPORTANCE_MAX);

            // Configure the notification channel.
            notificationChannel.setDescription("Caller info notification");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Notification.Builder notificationBuilder = new Notification.Builder(context, NOTIFICATION_CHANNEL_ID);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context,SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK), 0);
        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setCategory(Notification.CATEGORY_CALL)
                .setOngoing(true)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon( BitmapFactory.decodeResource(getResources(), R.drawable.ic_play_logo))
                .setTicker("Hearty365")
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new Notification.BigTextStyle().bigText(bigMessage))
                .setFullScreenIntent(pendingIntent,true)
                .setContentInfo(lastCallInfo);

        notificationManager.notify(/*notification id*/1, notificationBuilder.build());
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (handler!=null)
//            handler.removeCallbacksAndMessages(null);
//        try {
//            if (dialog != null && dialog.isShowing())
//                dialog.dismiss();
//            finish();
//        } catch (Exception e) {
//            finish();
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (handler!=null)
            handler.removeCallbacksAndMessages(null);
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();
            finish();
        } catch (Exception e) {
            finish();
        }
    }

    private void getLeadNameAPI() {
        if (Utility.isConnectingToInternet(context)) {
            LeadNameRequest ob = new LeadNameRequest();
            ob.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            ob.setPhone(SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER));
            RetrofitClient.getAPIService().getLeadNameAPI(ob).enqueue(new Callback<LeadNameResponse>() {
                @Override
                public void onResponse(Call<LeadNameResponse> call, Response<LeadNameResponse> response) {
                    if (response.code() == 200) {
                        SharedPref.setSharedPreference(context, Constants.SHOW_ADD_LEAD_DETAILS_FORM, String.valueOf(response.body().showForm()));
                        if (response.body().getName()!=null) {

                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                                    && Build.MANUFACTURER.toLowerCase().contains("xiaomi"))
                                showNotification(response.body(), response.body().getName(), response.body().getLast_contacted());
                            else
                                showDialogue(response.body(), response.body().getName(), response.body().getLast_contacted());
                        } else {
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                                    && Build.MANUFACTURER.toLowerCase().contains("xiaomi"))
                                showNotification(null, null, null);
                            else
                                showDialogue(null, null, null);
                        }
                    } else
                        finish();
                }

                @Override
                public void onFailure(Call<LeadNameResponse> call, Throwable t) {
                    Log.e(TAG, "getLeadNameAPI onFailure: " + t.getMessage());
                    finish();
                }
            });
        } else
            showDialogue(null, null, null);
    }
}