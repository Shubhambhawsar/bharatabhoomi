package com.dataignyte.bharathabhoomicrm.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.adapters.LeadInformationAdapter;
import com.dataignyte.bharathabhoomicrm.fragments.LeadCallHistoryFragment;
import com.dataignyte.bharathabhoomicrm.fragments.LeadDetailsFragment;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LeadDetailsActivity extends BaseActivity {

    private static final String TAG = LeadDetailsActivity.class.getSimpleName();
    @BindView(R.id.ivButtonBack)
    ImageView ivButtonBack;
    @BindView(R.id.txvTitle)
    TextView txvTitle;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.myViewpager)
    ViewPager myViewpager;

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_details);
        ButterKnife.bind(this);
        initialise();
    }

    private void initialise() {
        context = this;

        tabLayout.addTab(tabLayout.newTab().setText(R.string.details));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.call_logs));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(LeadDetailsFragment.getFragment(getIntent().getStringExtra(Constants._ID)));
        fragments.add(LeadCallHistoryFragment.getFragment(getIntent().getStringExtra(Constants._ID)));

        final MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), fragments);
        myViewpager.setAdapter(adapter);
        myViewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                myViewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick({R.id.ivButtonBack, R.id.ivButtonChat, R.id.tabLayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.ivButtonChat:
                startActivity(new Intent(context, LeadChatActivity.class)
                        .putExtra(Constants._ID, getIntent().getStringExtra(Constants._ID))
                        .putExtra(Constants.LEAD_NAME, getIntent().getStringExtra(Constants.LEAD_NAME)));
                break;
            case R.id.tabLayout:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LeadInformationAdapter.setEditable = false;
        stopProgressHud();
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Fragment> fragments;
        public MyPagerAdapter(FragmentManager supportFragmentManager, ArrayList<Fragment> fragments) {
            super(supportFragmentManager);
            this.fragments = fragments;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}