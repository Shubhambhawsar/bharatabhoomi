package com.dataignyte.bharathabhoomicrm.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.dataignyte.bharathabhoomicrm.BuildConfig;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.fragments.AboutFragments;
import com.dataignyte.bharathabhoomicrm.fragments.DashBoardFragment;
import com.dataignyte.bharathabhoomicrm.fragments.DocumentsFragment;
import com.dataignyte.bharathabhoomicrm.fragments.LeadsListFragment;
import com.dataignyte.bharathabhoomicrm.fragments.ProfileFragments;
import com.dataignyte.bharathabhoomicrm.fragments.SupportFragment;
import com.dataignyte.bharathabhoomicrm.fragments.UntrackedLeadsListFragment;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.GetDasBoardRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.FileUploadResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.FOREGROUND_SERVICE;
import static android.Manifest.permission.MODIFY_AUDIO_SETTINGS;
import static android.Manifest.permission.PROCESS_OUTGOING_CALLS;
import static android.Manifest.permission.READ_CALL_LOG;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission_group.STORAGE;

public class MainActivitySideDrawer extends BaseActivity {
    private static final String TAG = MainActivitySideDrawer.class.getSimpleName();
    @BindView(R.id.sideDrawer)
    DrawerLayout sideDrawer;
    @BindView(R.id.ivButtonSearch)
    ImageView ivButtonSearch;
    @BindView(R.id.ivButtonRefresh)
    ImageView ivButtonRefresh;
//    @BindView(R.id.ivProfilePic)
//    ImageView ivProfilePic;
//    @BindView(R.id.txvName)
//    TextView txvName;
//    @BindView(R.id.txvEmail)
//    TextView txvEmail;
    @BindView(R.id.txvToolbarTittle)
    public TextView txvToolbarTittle;
    @BindView(R.id.llAddNew)
    LinearLayout llAddNew;
    @BindView(R.id.llAddFromCallLog)
    LinearLayout llAddFromCallLog;
    public static EditText edtSearch;

    private Context context;
    private Fragment selectedFragment = new Fragment();
    private BroadcastReceiver myBroadcastReceiver;
    private boolean isFatBtnOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initialise();
    }

    private void initialise() {
        context = this;
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        startMyBroadcastReciever();
        if (!Utility.checkRuntimePermission(this, READ_PHONE_STATE, PROCESS_OUTGOING_CALLS, CALL_PHONE, READ_CALL_LOG, FOREGROUND_SERVICE, RECORD_AUDIO, MODIFY_AUDIO_SETTINGS, STORAGE, WRITE_EXTERNAL_STORAGE, READ_CONTACTS)) {
            ActivityCompat.requestPermissions(this, new String[]{READ_PHONE_STATE, PROCESS_OUTGOING_CALLS, CALL_PHONE, READ_CALL_LOG, FOREGROUND_SERVICE, RECORD_AUDIO, MODIFY_AUDIO_SETTINGS, STORAGE, WRITE_EXTERNAL_STORAGE, READ_CONTACTS}, 1101);
        } else {
            registerCallReceiver();
        }
        setFragments(new DashBoardFragment(), null, false);

//        txvName.setText(SharedPref.getSharedPreferences(context, Constants.USER_NAME));
//        txvEmail.setText(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
//        ((TextView) findViewById(R.id.txvAppVersion)).setText("App V: " + BuildConfig.VERSION_NAME);

//        if (SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC) !=null
//                && SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC) .length()>10)
//            Glide.with(context).load(SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC)).into(ivProfilePic);
    }

    public void setFragments(Fragment fragment, Bundle bundle, boolean addToBackStack) {
        if (fragment instanceof DashBoardFragment) {
            txvToolbarTittle.setText(R.string.dashboard);
            ivButtonSearch.setVisibility(View.GONE);
            ivButtonRefresh.setVisibility(View.GONE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setVisibility(View.GONE);
        } else if (fragment instanceof LeadsListFragment) {
            txvToolbarTittle.setText(R.string.myLeads);
            ivButtonSearch.setVisibility(View.VISIBLE);
            ivButtonRefresh.setVisibility(View.GONE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setVisibility(View.VISIBLE);
        } else if (fragment instanceof UntrackedLeadsListFragment) {
            txvToolbarTittle.setText(R.string.un_tracked_leads);
            ivButtonSearch.setVisibility(View.VISIBLE);
            ivButtonRefresh.setVisibility(View.GONE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setVisibility(View.VISIBLE);
        } else if (fragment instanceof SupportFragment) {
            txvToolbarTittle.setText(R.string.support);
            ivButtonRefresh.setVisibility(View.VISIBLE);
            ivButtonSearch.setVisibility(View.GONE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setVisibility(View.GONE);
        } else {
            ivButtonRefresh.setVisibility(View.GONE);
            edtSearch.setVisibility(View.GONE);
            ivButtonSearch.setVisibility(View.GONE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setVisibility(View.GONE);
        }
        selectedFragment = fragment;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (bundle != null)
            fragment.setArguments(bundle);
        transaction.replace(R.id.framLayout, fragment, fragment.getClass().getName());
        if (addToBackStack)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();

//        if (SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC) !=null
//                && SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC) .length()>10)
//            Glide.with(context).load(SharedPref.getSharedPreferences(context, Constants.PROFILE_PIC)).into(ivProfilePic);
    }

//    @OnClick({R.id.ivButtonSearch, R.id.ivButtonRefresh, R.id.ivButtonDrawer, R.id.rlDrawerBtnMyLeads,  R.id.rlDrawerBtnUntrackedLead, R.id.rlDrawerBtnProfile, R.id.rlDrawerBtnDocument, R.id.rlDrawerBtnSupport, R.id.rlDrawerBtnAbout, R.id.rlDrawerBtnogout, R.id.fatBtnAddLead, R.id.llAddFromCallLog, R.id.llAddNew})
    @OnClick({R.id.ivButtonSearch, R.id.ivButtonRefresh, R.id.ivButtonDrawer, R.id.fatBtnAddLead, R.id.llAddFromCallLog, R.id.llAddNew})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivButtonSearch:
                if (edtSearch.isShown()) {
                    edtSearch.setText("");
                    txvToolbarTittle.setVisibility(View.VISIBLE);
                    edtSearch.setVisibility(View.GONE);
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
                } else {
                    edtSearch.setText("");
                    txvToolbarTittle.setVisibility(View.GONE);
                    edtSearch.setVisibility(View.VISIBLE);
                    edtSearch.requestFocus();
                    ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(edtSearch, InputMethodManager.SHOW_IMPLICIT);
                }
                break;

            case R.id.ivButtonRefresh:
                if (selectedFragment instanceof SupportFragment)
                    ((SupportFragment) selectedFragment).getSupportTeamMessagesAPI();
                break;

            case R.id.fatBtnAddLead:
                showAllButton();
                break;
            case R.id.llAddNew:
                startActivity(new Intent(context, AddLeadActivity.class));
                showAllButton();
                break;
            case R.id.llAddFromCallLog:
                startActivity(new Intent(context, CallLogActivity.class));
                showAllButton();
                break;

            case R.id.ivButtonDrawer:
                sideDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.rlDrawerBtnMyLeads:
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new LeadsListFragment(), null, false);
                break;
            case R.id.rlDrawerBtnUntrackedLead:
                txvToolbarTittle.setText(R.string.un_tracked_leads);
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new UntrackedLeadsListFragment(), null, false);
                break;
            case R.id.rlDrawerBtnProfile:
                txvToolbarTittle.setText(R.string.profile);
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new ProfileFragments(), null, false);
                break;
            case R.id.rlDrawerBtnDocument:
                txvToolbarTittle.setText(R.string.documents);
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new DocumentsFragment(), null, false);
                break;
            case R.id.rlDrawerBtnSupport:
                txvToolbarTittle.setText(R.string.support);
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new SupportFragment(), null, false);
                break;
            case R.id.rlDrawerBtnAbout:
                txvToolbarTittle.setText(R.string.about);
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new AboutFragments(), null, false);
                break;
            case R.id.rlDrawerBtnogout:
                logoutDialogue();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        selectedFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerCallReceiver();
            } else {
                ActivityCompat.requestPermissions(this, permissions, 1101);
                Toast.makeText(this, "Permissions Needed", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(selectedFragment!=null)
        selectedFragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter ifillter = new IntentFilter();
        LocalBroadcastManager.getInstance(this).registerReceiver(myBroadcastReceiver, ifillter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter ifillter = new IntentFilter();
        ifillter.addAction("Call");
        LocalBroadcastManager.getInstance(this).registerReceiver(myBroadcastReceiver, ifillter);
    }

    @Override
    public void onBackPressed() {
        if (selectedFragment instanceof DashBoardFragment) {
            new AlertDialog.Builder(context)
                    .setMessage(R.string.do_you_want_to_exit)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        } else
            setFragments(new DashBoardFragment(), null, false);
    }

    public void logoutDialogue() {
        new AlertDialog.Builder(context)
                .setMessage(R.string.you_want_to_exit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callLogoutApi();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();

    }

    private void startMyBroadcastReciever() {
        myBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(MainActivitySideDrawer.class.getSimpleName(), "onReceive Local Broadcast: ");
            }
        };
    }

    private void showAllButton() {
        if (!isFatBtnOpen) {
            isFatBtnOpen = true;
            llAddFromCallLog.animate().translationX(-0).alpha(1.0f).setListener(null);
            llAddNew.animate().translationX(-0).alpha(1.0f).setListener(null);
            llAddNew.setVisibility(View.VISIBLE);
            llAddFromCallLog.setVisibility(View.VISIBLE);
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close));
        } else {
            isFatBtnOpen = false;
            ((FloatingActionButton)findViewById(R.id.fatBtnAddLead)).setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add));
            llAddNew.animate().translationX(0).alpha(0f).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    llAddNew.setVisibility(View.GONE);
                }
            });
            llAddFromCallLog.animate().translationX(0).alpha(0f).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    llAddFromCallLog.setVisibility(View.GONE);
                }
            });
        }
    }

    private void callLogoutApi() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            GetDasBoardRequest object = new GetDasBoardRequest();
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            RetrofitClient.getAPIService().callLogoutAPI(object).enqueue(new Callback<FileUploadResponse>() {
                @Override
                public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {
                        if (response.body().getSuccess() != null) {
                            sideDrawer.closeDrawer(GravityCompat.START);
                            SharedPref.clearPreference(context);
                            unRegisterCallReceiver();
                            startActivity(new Intent(context, LoginActivity.class));
                            finish();
                        } else {
                            Utility.ShowToastMessage(context, getString(R.string.enter_valid_credentials));
                        }
                    }
                }

                @Override
                public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "callLoginAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }
}