package com.dataignyte.bharathabhoomicrm.activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.CreateNewLeadRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.AddLeadResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.FileUploadResponse;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLeadActivity extends BaseActivity {

    private static final String TAG = AddLeadActivity.class.getSimpleName();
    @BindView(R.id.ivButtonBack)
    ImageView ivButtonBack;
    @BindView(R.id.edtFirstName)
    EditText edtFirstName;
    @BindView(R.id.edtLastName)
    EditText edtLastName;
    @BindView(R.id.edtNumber)
    EditText edtNumber;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPlace)
    EditText edtPlace;
    @BindView(R.id.edtCity)
    EditText edtCity;
    @BindView(R.id.spnProjects)
    Spinner spnProjects;
    @BindView(R.id.edtScores)
    EditText edtScores;
    @BindView(R.id.edtOccupation)
    EditText edtOccupation;
    @BindView(R.id.spnSource)
    Spinner spnSource;
    @BindView(R.id.edtRemarks)
    EditText edtRemarks;
    @BindView(R.id.txvButtonSave)
    TextView txvButtonSave;
    @BindView(R.id.txvButtonCancel)
    TextView txvButtonCancel;

    public Context context;
    private String call_duration = "", enqDate = "";
    private boolean isFromCallLog = false;

    String[] leadSourceList = {"Select Lead Source:"
            , "SSTV"
            , "Banner"
            , "Broucher distribution"
            , "Pomplet distribution"
            , "Advertisement in Some media"
            , "Website"
            , "facebook"
            , "W/A"
            , "Relatives"
            , "Existing client"
            , " Client manager own effort",},
            projectsList = {"Select Project:"
                    , "BBT"
                    , "SSC"
                    , "BBT/SSC"
                    , "Kuberapuram"
                    , "Others"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lead);
        ButterKnife.bind(this);

        initialise();
    }

    private void initialise() {
        context = this;
        if (getIntent().hasExtra(Constants.LEAD_NUMBER)) {
            String phone = getIntent().getStringExtra(Constants.LEAD_NUMBER);
            if (phone.length()==13)
                phone = phone.substring(3);
            edtNumber.setText(phone);
            isFromCallLog = true;
        }
        if (getIntent().hasExtra(Constants.CALL_DURATION))
            call_duration = getIntent().getStringExtra(Constants.CALL_DURATION);

        if (getIntent().hasExtra(Constants.CALL_DATE))
            enqDate = getIntent().getStringExtra(Constants.CALL_DATE);

        if (getIntent().hasExtra(Constants.LEAD_NAME))
            edtLastName.setText(getIntent().getStringExtra(Constants.LEAD_NAME));

        ArrayAdapter spnProjectsAdapter = new ArrayAdapter(context, R.layout.spinner_textview, R.id.txvValue, projectsList);
        spnProjectsAdapter.setDropDownViewResource(R.layout.spinner_textview);
        spnProjects.setAdapter(spnProjectsAdapter);

        ArrayAdapter spnLeadSourceAdapter = new ArrayAdapter(context, R.layout.spinner_textview, R.id.txvValue, leadSourceList);
        spnLeadSourceAdapter.setDropDownViewResource(R.layout.spinner_textview);
        spnSource.setAdapter(spnLeadSourceAdapter);
    }

    @OnClick({R.id.ivButtonBack, R.id.txvButtonSave, R.id.txvButtonCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txvButtonCancel:
            case R.id.ivButtonBack:
                finish();
                break;

            case R.id.txvButtonSave:
                if (edtLastName.getText().toString().trim().isEmpty())
                    edtLastName.setError(getString(R.string.enter_name));
                else if (edtNumber.getText().toString().trim().isEmpty()) {
                    edtNumber.setError(getResources().getString(R.string.enter_number));
                    Utility.ShowToastMessage(context, getResources().getString(R.string.enter_number));
                } else if (edtNumber.getText().toString().trim().length() < 10) {
                    edtNumber.setError(getResources().getString(R.string.enter_valid_number));
                    Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_number));
                } else if (!edtEmail.getText().toString().trim().isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches())
                    Utility.ShowToastMessage(context, getResources().getString(R.string.enter_valid_email_address));
                else
                    callAddNewLeadeAPI(SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER), "");
                break;
        }
    }

    private void callAddNewLeadeAPI(String number, String note) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();

            CreateNewLeadRequest requestObject = new CreateNewLeadRequest();
            requestObject.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            requestObject.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            requestObject.setAdded_through_call_log(isFromCallLog);
            requestObject.setPhone(edtNumber.getText().toString().trim());
            requestObject.setCalled_to(edtNumber.getText().toString().trim());
            requestObject.setCall_duration(call_duration);
            requestObject.setNote(note);
            requestObject.setFirst_Name(edtFirstName.getText().toString().trim());
            requestObject.setLast_Name(edtLastName.getText().toString().trim());
            requestObject.setFull_Name(edtLastName.getText().toString().trim() );
            requestObject.setEmail(edtEmail.getText().toString().trim());
            requestObject.setPlace(edtPlace.getText().toString().trim());
            requestObject.setCity(edtCity.getText().toString().trim());
            requestObject.setProjects(getSpinnerSelcetedItem(spnProjects));
            requestObject.setRemarks(edtRemarks.getText().toString().trim());
            requestObject.setScores(edtScores.getText().toString().trim());
            requestObject.setOCCUPATION(edtOccupation.getText().toString().trim());
            requestObject.setLead_Source(getSpinnerSelcetedItem(spnSource));
            requestObject.setLead_Status("");
            requestObject.setForm(Constants.TRUE);
            requestObject.setEnq_Date(enqDate);

            RetrofitClient.getAPIService().addNewLeadAPI(requestObject).enqueue(new Callback<AddLeadResponse>() {
                @Override
                public void onResponse(Call<AddLeadResponse> call, Response<AddLeadResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {
                        if (response.body().getSuccess() != null) {
                            Utility.ShowToastMessage(context, response.body().getSuccess());
                            finish();
                        } else
                            Utility.ShowToastMessage(context, response.body().getError());
                    }
                }

                @Override
                public void onFailure(Call<AddLeadResponse> call, Throwable t) {
                    Utility.ShowToastMessage(context, "Network problem! try again....");
                    stopProgressHud();
                    Log.e(TAG, "callUploadLeadLogFileAPI onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private String getSpinnerSelcetedItem(Spinner spinner) {
        if (spinner.getSelectedItemPosition() == 0)
            return "";
        else
            return (String) spinner.getSelectedItem();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopProgressHud();
    }
}