package com.dataignyte.bharathabhoomicrm.activities;

import androidx.core.app.ActivityCompat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import com.dataignyte.bharathabhoomicrm.BuildConfig;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.AutoStartHelper;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.CheckApkVersionRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.ApkVersionResponse;
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.INTERNET;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        if (!Utility.checkRuntimePermission(context, INTERNET, ACCESS_NETWORK_STATE, ACCESS_WIFI_STATE)) {
            ActivityCompat.requestPermissions(this, new String[]{INTERNET, ACCESS_NETWORK_STATE, ACCESS_WIFI_STATE}, 1101);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)) {
            new AlertDialog.Builder(context)
                    .setMessage(getResources().getString(R.string.message_enable_access_overaly))
                    .setCancelable(false)
                    .setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent myIntent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                            myIntent.setData(Uri.parse("package:" + getPackageName()));
                            startActivity(myIntent);
                        }
                    })
                    .show();
        } else if (!Utility.isAccessibilitySettingsOn(context)) {
            new AlertDialog.Builder(context)
                    .setMessage(getResources().getString(R.string.message_enable_accessiblity_permission))
                    .setCancelable(false)
                    .setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent openSettings = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            openSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            startActivity(openSettings);
                        }
                    })
                    .show();
        } else if (SharedPref.getSharedPreferences(context, Constants.APP_AUTO_START) == null || !SharedPref.getSharedPreferences(context, Constants.APP_AUTO_START).equalsIgnoreCase(Constants.TRUE)) {

            AutoStartHelper.getInstance().getAutoStartPermission(this);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkBatteryOptimisation();
        } else {
            if (SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID).length() > 0) {
                getLatestVersionOfAPK();
            } else {
                splashScreenHandler(new Intent(context, LoginActivity.class));
            }
        }
    }

    private void checkBatteryOptimisation() {
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        if (pm != null && !pm.isIgnoringBatteryOptimizations(getPackageName())) {
            askIgnoreOptimization();
        } else {
            if (SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID).length() > 0) {
                getLatestVersionOfAPK();
            } else {
                splashScreenHandler(new Intent(context, LoginActivity.class));
            }
        }
    }

    private void askIgnoreOptimization() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
        } else {
            if (SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID).length() > 0) {
                getLatestVersionOfAPK();
            } else {
                splashScreenHandler(new Intent(context, LoginActivity.class));
            }
        }
    }

    private void getLatestVersionOfAPK() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            CheckApkVersionRequest object = new CheckApkVersionRequest();
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            RetrofitClient.getAPIService().getLatestVersionOfAPK(object).enqueue(new Callback<ApkVersionResponse>() {
                @Override
                public void onResponse(Call<ApkVersionResponse> call, Response<ApkVersionResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200) {

                        if (response.body().getApk_version() != null) {
                            if (BuildConfig.VERSION_NAME.equalsIgnoreCase(response.body().getApk_version())) {

                                splashScreenHandler(new Intent(context, MainActivitySideDrawer.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            } else {
                                new AlertDialog.Builder(context)
                                        .setTitle(context.getResources().getString(R.string.app_name))
                                        .setMessage(getResources().getString(R.string.you_have_version_availablle))
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.enable, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent i = new Intent(Intent.ACTION_VIEW);
                                                i.setData(Uri.parse(response.body().getApk_url()));
                                                startActivity(i);
                                            }
                                        })
                                        .show();
                            }
                        } else {
                            splashScreenHandler(new Intent(context, MainActivitySideDrawer.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ApkVersionResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, "getLeadInfoDetails onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getString(R.string.no_internet_connection));
    }

    private void splashScreenHandler(Intent intent) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, 3000);

    }
}