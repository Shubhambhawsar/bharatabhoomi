package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApkVersionResponse {

    @SerializedName(Constants.APK_VERSION)
    @Expose
    private String apk_version;

    @SerializedName(Constants.APK_URL)
    @Expose
    private String apk_url ;

    @SerializedName(Constants.DATE_ADDED)
    @Expose
    private String date_added;


    public String getApk_version() {
        return apk_version;
    }

    public String getApk_url() {
        return apk_url;
    }

    public String getDate_added() {
        return date_added;
    }
}
