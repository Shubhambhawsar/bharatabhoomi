package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditFieldObject {

    @SerializedName(Constants.NAME)
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;

    public EditFieldObject(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
