package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName(Constants.USEREMAIL)
    @Expose
    private String useremail;

    @SerializedName(Constants.PASSWORD)
    @Expose
    private String password;

    @SerializedName(Constants.DEVICE_ID)
    @Expose
    private String device_id;

    @SerializedName(Constants.MOBILE_VERSION)
    @Expose
    private String mobile_version;

    @SerializedName(Constants.ANDROID_VERSION)
    @Expose
    private String android_version;

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMobile_version() {
        return mobile_version;
    }

    public void setMobile_version(String mobile_version) {
        this.mobile_version = mobile_version;
    }

    public String getAndroid_version() {
        return android_version;
    }

    public void setAndroid_version(String android_version) {
        this.android_version = android_version;
    }
}
