package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDashBoardResponse {

    @SerializedName("Full_Name")
    @Expose
    private String Full_Name;

    @SerializedName("Email")
    @Expose
    private String Email;

    //    @SerializedName(Constants.PHONE)
    @SerializedName("Mobile_1")
    @Expose
    private String Phone;

    @SerializedName("last_contacted_time")
    @Expose
    private String last_contacted_time;

    @SerializedName("_id")
    @Expose
    private String _id;

    @SerializedName("lead_id")
    @Expose
    private String lead_id;

    @SerializedName("Mobile")
    @Expose
    private String Mobile;

    @SerializedName("Mobile_2")
    @Expose
    private String W_A_number;

    public String getFull_Name() {
        return Full_Name;
    }

    public String get_id() {
        return _id;
    }

    public String getPhone() {
        return Phone;
    }

    public String getW_A_number() {
        return W_A_number;
    }

    public String getLast_contacted_time() {
        return last_contacted_time;
    }

    public String getEmail() {
        return Email;
    }

    public String getLead_id() {
        return lead_id;
    }

    public String getMobile() {
        return Mobile;
    }
}
