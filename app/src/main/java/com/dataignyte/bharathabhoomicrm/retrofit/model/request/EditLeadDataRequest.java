package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EditLeadDataRequest {

    @SerializedName(Constants.CUSTOMER_ID)
    @Expose
    private int customer_id;

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.USER_ROLE)
    @Expose
    private String user_role;

    @SerializedName(Constants.LEAD_ID)
    @Expose
    private String lead_id;

    @SerializedName(Constants.EDITFIELD)
    @Expose
    private ArrayList<EditFieldObject> editfield;

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public void setEditfield(ArrayList<EditFieldObject> editfield) {
        this.editfield = editfield;
    }
}
