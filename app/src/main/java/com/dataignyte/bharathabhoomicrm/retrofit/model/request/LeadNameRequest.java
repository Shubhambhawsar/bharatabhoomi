package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadNameRequest {

    @SerializedName(Constants.CUSTOMER_ID)
    @Expose
    private int customer_id;

    @SerializedName(Constants.PHONE)
    @Expose
    private String phone;

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
