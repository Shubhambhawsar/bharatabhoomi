package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DocumentsResponse {

    @SerializedName("aadharCardUrl")
    @Expose
    private String aadharCardUrl;

    @SerializedName("panCardUrl")
    @Expose
    private String panCardUrl;

    @SerializedName("profilePicUrl")
    @Expose
    private String profilePicUrl;

    public String getAadharCardUrl() {
        return Utility.geNonNullString(aadharCardUrl);
    }

    public String getPanCardUrl() {
        return Utility.geNonNullString(panCardUrl);
    }

    public String getProfilePicUrl() {
        return Utility.geNonNullString(profilePicUrl);
    }
}
