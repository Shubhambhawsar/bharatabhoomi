package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadInfoData {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("editable")
    @Expose
    private boolean editable;

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
