package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConversationMessage {
    @SerializedName(Constants.USERNAME)
    @Expose
    private String username;

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.TEXT)
    @Expose
    private String text;

    @SerializedName(Constants.TIME)
    @Expose
    private String time;

    @SerializedName(Constants.DATE)
    @Expose
    private String date;

    @SerializedName(Constants.USER_ROLE)
    @Expose
    private String user_role;

    public String getUsername() {
        return username;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

    public String getUser_role() {
        return user_role;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
