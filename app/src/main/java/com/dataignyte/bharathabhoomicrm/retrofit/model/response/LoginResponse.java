package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName(Constants.CUSTOMER_ID)
    @Expose
    private int customer_id;

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.USER_ROLE)
    @Expose
    private String user_role;

    @SerializedName(Constants.USER_NAME)
    @Expose
    private String user_name;

    @SerializedName(Constants.STATUS_ID)
    @Expose
    private int status_id;

    public int getCustomer_id() {
        return customer_id;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_role() {
        return user_role;
    }

    public String getUser_name() {
        return user_name;
    }

    public int getStatus_id() {
        return status_id;
    }
}
