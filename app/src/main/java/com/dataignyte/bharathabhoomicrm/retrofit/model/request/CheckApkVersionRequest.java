package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckApkVersionRequest {

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
}
