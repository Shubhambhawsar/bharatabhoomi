package com.dataignyte.bharathabhoomicrm.retrofit.APICaller;

import com.dataignyte.bharathabhoomicrm.retrofit.model.request.*;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.*;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface ApiInterface {
    @POST(Constants.Api_URL.LOGIN_URL)
    Call<LoginResponse> callLoginAPI( @Body LoginRequest requestObject);

    @POST(Constants.Api_URL.LOGOUT_URL)
    Call<FileUploadResponse> callLogoutAPI(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.UPLOAD_LEAD_LOG_FILE_URL)
    Call<FileUploadResponse> callUploadLeadLogFileAPI(@Body RequestBody requestObject);

    @POST(Constants.Api_URL.UPLOAD_LEAD_LOG_FILE_URL)
    Call<FileUploadResponse> callUploadFromLeadCallLogAPI(@Body RequestBody requestObject);

    @POST(Constants.Api_URL.GET_LEAD_NAME_URL)
    Call<LeadNameResponse> getLeadNameAPI(@Body LeadNameRequest requestObject);

    @POST(Constants.Api_URL.GET_LEAD_DASHBOARD_URL)
    Call<ArrayList<GetDashBoardResponse>> getDashBoardLeadsList(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.GET_UNTRACKED_LEAD_DASHBOARD_URL)
    Call<ArrayList<GetDashBoardResponse>> getUntrackedLeadsList(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.GET_LEAD_INFO_DETAILS_URL)
    Call<GetLeadDetaiResponse> getLeadInfoDetails(@Body GetLeadDetaiRequest requestObject);

    @POST(Constants.Api_URL.GET_UNTRACKED_LEAD_DATA_URL)
    Call<GetLeadDetaiResponse> getUntrackedLeadInfoDetails(@Body GetLeadDetaiRequest requestObject);

    @POST(Constants.Api_URL.EDIT_LEAD_DETAILS_URL)
    Call<SuccessResponse> callUpdateLeadDetailsAPI(@Body EditLeadDataRequest requestObject);

    @POST(Constants.Api_URL.EDIT_UNTRACKED_LEAD_DETAILS_URL)
    Call<SuccessResponse> callUpdateUntrackedLeadDetailsAPI(@Body EditLeadDataRequest requestObject);

    @POST(Constants.Api_URL.GET_LATEST_VERSION_OF_APP_URL)
    Call<ApkVersionResponse> getLatestVersionOfAPK(@Body CheckApkVersionRequest requestObject);

    @POST(Constants.Api_URL.UPDATE_MISSCALL_DETAIL_URL)
    Call<SuccessResponse> callUpdateMissCallDetailsAPI(@Body UpdateMissCallDetailRequest requestObject);

    @POST(Constants.Api_URL.GET_AUDIT_CALL_LOG_URL)
    Call<ArrayList<CallLogResponse>> getAgentsCallLogAPI(@Body GetLeadDetaiRequest requestObject);

    @POST(Constants.Api_URL.CREATE_NEW_LEAD_URL)
    Call<AddLeadResponse> addNewLeadAPI(@Body CreateNewLeadRequest requestObject);

    @POST(Constants.Api_URL.GET_PROFILE_URL)
    Call<GetProfileResponse> getUserProfileDetailsAPI(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.ADD_PROFILE_PIC_URL)
    Call<FileUploadResponse> addUserProfilePictureAPI(@Body RequestBody requestObject);

    @POST(Constants.Api_URL.UPLOAD_DOCUMENT_URL)
    Call<FileUploadResponse> uploadDocumentAPI(@Body RequestBody requestObject);

    @POST(Constants.Api_URL.GET_LEAD_AGENT_CHAT_URL)
    Call<LeadAgentConversationResponse> getLeadAgentConversationMessagesAPI(@Body LeadAgentConversationRequest requestObject);

    @POST(Constants.Api_URL.SEND_MESSAGE_URL)
    Call<SuccessResponse> callSendMessagesAPI(@Body SendMessageRequest requestObject);

    @POST(Constants.Api_URL.GET_SUPPORT_DISCUSSION_URL)
    Call<ArrayList<ConversationMessage>> getSupportTeamMessagesAPI(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.GET_DOCUMENTS_URL)
    Call<DocumentsResponse> getDocumentsAPI(@Body GetDasBoardRequest requestObject);

    @POST(Constants.Api_URL.SEND_QUERY_MESSAGE_URL)
    Call<SuccessResponse> callSendQueryMessagesAPI(@Body SendMessageRequest requestObject);
}