package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLeadDetaiRequest {

    @SerializedName("customer_id")
    @Expose
    private int customer_id;

    @SerializedName("lead_id")
    @Expose
    private String lead_id;

    @SerializedName("user_email")
    @Expose
    private String user_email;

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
}
