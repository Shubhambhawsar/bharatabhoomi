package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CallLogObject{

    @SerializedName(Constants.NAME)
    @Expose
    private String name;

    @SerializedName(Constants.PHONE)
    @Expose
    private String phoneNumber;

    @SerializedName(Constants.CALL_TYPE)
    @Expose
    private String call_type;

    @SerializedName(Constants.CALL_DURATION)
    @Expose
    private String call_duration;

    @SerializedName(Constants.START_TIME)
    @Expose
    private String start_time;

    @SerializedName(Constants.START_TIME)
    @Expose
    private Date dateTime;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
