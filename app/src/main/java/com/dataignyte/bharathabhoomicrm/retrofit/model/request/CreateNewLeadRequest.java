package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import android.provider.ContactsContract;

import com.dataignyte.bharathabhoomicrm.activities.BaseActivity;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class CreateNewLeadRequest {
    @SerializedName("customer_id")
    @Expose
    private int customer_id;
    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;
    @SerializedName(Constants.ADDED_THROUGH_CALL_LOG)
    @Expose
    private boolean added_through_call_log;

//        @SerializedName(Constants.PHONE)
//    @SerializedName("Mobile_1")                   // 07/12/2020
//    @SerializedName("mobile")                     // 08/12/2020
    @SerializedName("Mobile")
    @Expose
    private String Phone;
    @SerializedName(Constants.LAST_NAME)
    @Expose
    private String Last_Name;

    @SerializedName(Constants.CALLED_TO)
    @Expose
    private String called_to;
    @SerializedName(Constants.CALL_DURATION)
    @Expose
    private String call_duration;
    @SerializedName(Constants.NOTES)
    @Expose
    private String note;

    @SerializedName("Full_Name")
    @Expose
    private String Full_Name;

    @SerializedName(Constants.FIRST_NAME)
    @Expose
    private String First_Name;
    @SerializedName(Constants.EMAIL)
    @Expose
    private String Email;
    @SerializedName(Constants.PLACE)
    @Expose
    private String place;
    @SerializedName(Constants.CITY)
    @Expose
    private String City;
    @SerializedName(Constants.PROJECTS)
    @Expose
    private String Projects;

//    @SerializedName(Constants.REMARKS)                // 07/12/2020
    @SerializedName("Non_Call_Note")
    @Expose
    private String Remarks;
    @SerializedName(Constants.SCORES)
    @Expose
    private String Scores;
    @SerializedName(Constants.OCCUPATION)
    @Expose
    private String OCCUPATION;
    @SerializedName(Constants.LEAD_SOURCE)
    @Expose
    private String Lead_Source;
    @SerializedName(Constants.LEAD_STATUS)
    @Expose
    private String Lead_Status = "";
    @SerializedName(Constants.FORM)
    @Expose
    private String form = Constants.TRUE;
    @SerializedName(Constants.ENQ_DATE)
    @Expose
    private String Enq_Date = "";

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setAdded_through_call_log(boolean added_through_call_log) {
        this.added_through_call_log = added_through_call_log;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public void setCalled_to(String called_to) {
        this.called_to = called_to;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setCity(String city) {
        City = city;
    }

    public void setProjects(String projects) {
        Projects = projects;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public void setScores(String scores) {
        Scores = scores;
    }

    public void setOCCUPATION(String OCCUPATION) {
        this.OCCUPATION = OCCUPATION;
    }

    public void setLead_Source(String lead_Source) {
        Lead_Source = lead_Source;
    }

    public void setLead_Status(String lead_Status) {
        Lead_Status = lead_Status;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public void setEnq_Date(String enq_Date) {
        Enq_Date = enq_Date;
    }

    public void setFull_Name(String full_Name) {
        Full_Name = full_Name;
    }
}
