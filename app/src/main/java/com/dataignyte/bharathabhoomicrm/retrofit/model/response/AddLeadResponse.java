package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddLeadResponse {
    @SerializedName("Error")
    @Expose
    private String Error;

    @SerializedName("Success")
    @Expose
    private String success;

    public String getError() {
        return Error;
    }

    public String getSuccess() {
        return success;
    }


}
