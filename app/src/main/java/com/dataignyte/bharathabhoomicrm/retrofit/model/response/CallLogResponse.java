package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallLogResponse {

    @SerializedName(Constants.STATUS)
    @Expose
    private String status;

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.USER_ROLE)
    @Expose
    private String user_role;

    @SerializedName(Constants.USER_NAME)
    @Expose
    private String user_name;

    @SerializedName(Constants.CALL_DURATION)
    @Expose
    private String call_duration;

    @SerializedName(Constants.CALLED_TO)
    @Expose
    private String called_to;

    @SerializedName(Constants.CALLED_TO_NAME)
    @Expose
    private String called_to_name;

    @SerializedName(Constants.NOTES)
    @Expose
    private String notes;

    @SerializedName(Constants.DRIVE_URL)
    @Expose
    private String drive_url;

    @SerializedName(Constants.VOICE_URL)
    @Expose
    private String voice_url;

    @SerializedName(Constants.FILE_URL)
    @Expose
    private String file_url;

    @SerializedName(Constants.CURRENT_TIME)
    @Expose
    private String current_time;

    @SerializedName(Constants.CALL_TYPE)
    @Expose
    private String call_type;

//    @SerializedName("file_url")
//    @Expose
//    private String file_url;

    public String getStatus() {
        return status;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_role() {
        return user_role;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public String getCalled_to() {
        return called_to;
    }

    public String getCalled_to_name() {
        return called_to_name;
    }

    public String getNotes() {
        return notes;
    }

    public String getDrive_url() {
        return drive_url;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public String getCall_type() {
        return call_type;
    }

    public String getVoice_url() {
        return voice_url;
    }

    public String getFile_url() {
        return file_url;
    }
}
