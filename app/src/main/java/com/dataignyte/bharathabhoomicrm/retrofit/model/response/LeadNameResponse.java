package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeadNameResponse {
    @SerializedName("SL_NO")
    @Expose
    private String SL_NO;

    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Phone_number")
    @Expose
    private String Phone_number;

    @SerializedName("last_contacted")
    @Expose
    private String last_contacted;

    @SerializedName("associate_name")
    @Expose
    private String associate_name;

    @SerializedName("Projects")
    @Expose
    private String Projects;

    @SerializedName("place")
    @Expose
    private String place;

    @SerializedName("form")
    @Expose
    private boolean form;

    public String getName() {
        return Name;
    }

    public String getPhone_number() {
        return Phone_number;
    }

    public String getLast_contacted() {
        return last_contacted;
    }

    public boolean showForm() {
        return form;
    }

    public String getSL_NO() {
        return SL_NO;
    }

    public String getAssociate_name() {
        return associate_name;
    }

    public String getProjects() {
        return Projects;
    }

    public String getPlace() {
        return place;
    }

    public boolean isForm() {
        return form;
    }
}
