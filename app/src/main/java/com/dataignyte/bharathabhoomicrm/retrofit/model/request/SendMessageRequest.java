package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendMessageRequest {

    @SerializedName(Constants.CUSTOMER_ID)
    @Expose
    private int customer_id;

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.LEAD_ID)
    @Expose
    private String lead_id;

    @SerializedName(Constants.MESSAGE)
    @Expose
    private String message;

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
