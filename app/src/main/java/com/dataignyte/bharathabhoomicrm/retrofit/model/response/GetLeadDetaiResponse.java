package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetLeadDetaiResponse {

    @SerializedName("lead_id")
    @Expose
    private String lead_id;
    @SerializedName("editable")
    @Expose
    private boolean editable;

    @SerializedName("stageObject")
    @Expose
    private StageObject stageObject;

    @SerializedName("data")
    @Expose
    private ArrayList<LeadInfoData> data = new ArrayList<>();

    public String getLead_id() {
        return lead_id;
    }

    public boolean isEditable() {
        return editable;
    }

    public ArrayList<LeadInfoData> getData() {
        return data;
    }

    public StageObject getStageObject() {
        return stageObject;
    }
}
