package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileUploadResponse {
    @SerializedName(Constants.ERROR)
    @Expose
    private String error;

    @SerializedName("success")
    @Expose
    private String success;

    public String getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }
}
