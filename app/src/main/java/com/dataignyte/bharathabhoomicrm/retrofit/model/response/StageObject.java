package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StageObject {
    @SerializedName("isStageVisible")
    @Expose
    boolean isStageVisible;
    @SerializedName("leadStage")
    @Expose
    String leadStage;
    @SerializedName("leadSubStage")
    @Expose
    String leadSubStage;

    public boolean isStageVisible() {
        return isStageVisible;
    }

    public String getLeadStage() {
        return leadStage;
    }

    public String getLeadSubStage() {
        return leadSubStage;
    }
}
