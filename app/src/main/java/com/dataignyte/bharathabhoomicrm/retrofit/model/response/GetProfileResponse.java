package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProfileResponse {
    @SerializedName("first_name")
    @Expose
    public String first_name;
    @SerializedName("last_name")
    @Expose
    public String last_name;
    @SerializedName("user_email")
    @Expose
    public String user_email;
    @SerializedName("user_role")
    @Expose
    public String user_role;
    @SerializedName("user_phone")
    @Expose
    public String user_phone;
    @SerializedName("user_address")
    @Expose
    public String user_address;
    @SerializedName("profile_pic_url")
    @Expose
    public String profile_pic_url;

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_role() {
        return user_role;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public String getUser_address() {
        return user_address;
    }

    public String getProfile_pic_url() {
        return profile_pic_url;
    }
}
