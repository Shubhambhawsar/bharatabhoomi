package com.dataignyte.bharathabhoomicrm.retrofit;

import java.util.ArrayList;

public class Constants {

    public static final ArrayList<String> LEAD_STAGE_ARRAY = new ArrayList<String>() {{
        add("Select lead stage:");
        add("Followup status");
        add("Site Visit");
        add("Negotiation");
    }};
    public static final ArrayList<String> LEAD_SUB_STAGE_FOLLOWUP_STATUS_ARRAY = new ArrayList<String>() {{
        add("Select sub-stage");
        add("CRM Called");
        add("SMS Sent");
        add("W/A sent");
        add("Introductory mail sent");
        add("Not contacted");
        add("Contacted, No response");
        add("Interested");
        add("Not interested");
        add("Invite for Zoom Call");
        add("Sent Invitation for zoom call");
        add("attended zoom call");
        add("zoom call, not attended");
        add("Customer Office Visit");
        add("Customer House visit done by CRM");
        add("Closed");
        add("Junk lead");
        add("Cold Lead");
        add("DND");
    }};
    public static final ArrayList<String> LEAD_SUB_STAGE_SITE_VISIT_ARRAY = new ArrayList<String>() {{
        add("Select sub-stage");
        add("Site Visit Interested");
        add("Schedule for Site Visit");
        add("Site visit Scheduled");
        add("Site visit Completed");
    }};
    public static final ArrayList<String> LEAD_SUB_STAGE_NEGOTIATION_ARRAY = new ArrayList<String>() {{
        add("Select sub-stage");
        add("Sale Concluded");
        add("Sale Lost");
    }};

    public static final String LEAD_NUMBER = "number";
    public static final String LEAD_NAME = "lead_name";
    public static final String START_TIME = "start_time";
    public static final String INFO = "info";
    public static final String CALL_TYPE = "call_type";
    public static final String CALL_TYPE_INCOMING = "Incoming Call";
    public static final String CALL_TYPE_OUT_GOING = "Outgoing Call";
    public static final String CALL_TYPE_MISSED_CALL = "Missed Call";

    public static final String ADHAAR = "adhaar_card";
    public static final String PAN_CARD = "pan_card";
    public static final String ENQ_DATE = "Enq_Date";
    public static final String END_TIME = "end_time";
    public static final String USER_EMAIL = "user_email";
    public static final String USEREMAIL = "useremail";
    public static final String PASSWORD = "password";
    public static final String DEVICE_ID = "device_id";
    public static final String MOBILE_VERSION = "mobile_version";
    public static final String ANDROID_VERSION = "android_version";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String USER_ROLE = "user_role";
    public static final String DATE = "date";
    public static final String USER_ROLE_INTERNAL_ASSOCIATE = "internal-associate";
    public static final String USER_NAME = "user_name";
    public static final String STATUS_ID = "status_id";
    public static final String FILE_PATH = "file_path";
    public static final String FILE_NAME = "file_name";
    public static final String FILE_SIZE = "file_size";
    public static final String FILE_URL = "file_url";
    public static final String LOG_TYPE = "Log_Type";
    public static final String LOG_TYPE_CALL = "call";
    public static final String LOG_TYPE_CALL_LOG = "call_log";
    public static final String CALLED_TO = "called_to";
    public static final String CALL_DURATION = "call_duration";
    public static final String CALL_DATE = "call_date";
    public static final String NOTES = "notes";
    public static final String NOTE_IMP_DISCUSSION = "important discussions";
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
    public static final String PHONE = "phone";
    public static final CharSequence MANUFACTURER_ONE_PLUS = "OnePlus";
    public static final String _ID = "_id";
    public static final String SHOW_ADD_LEAD_DETAILS_FORM = "show_add_lead_form_details";
    public static final String FIRST_NAME = "First_Name";
    public static final String LAST_NAME = "Last_Name";
    public static final String EMAIL = "Email";
    public static final String PLACE = "place";
    public static final String CITY = "City";
    public static final String PROJECTS = "Projects";
    public static final String REMARKS = "Remarks";
    public static final String SCORES = "Scores";
    public static final String OCCUPATION = "OCCUPATION";
    public static final String LEAD_SOURCE = "Lead_Source";
    public static final String LEAD_STATUS = "Lead_Status";
    public static final String LEAD_ID = "lead_id";
    public static final String EDITFIELD = "editfield";
    public static final String NAME = "name";
    public static final String FORM = "form";
    public static final String VOICE_RECORD_URL = "voice_record_url";
    public static final String APK_VERSION = "apk_version";
    public static final String APK_URL = "apk_url";
    public static final String DATE_ADDED = "date_added";
    public static final String APP_AUTO_START = "app_auto_start";
    public static final String TRUE = "true";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String LEADS_PROJECT = "leads_project";
    public static final String STATUS = "status";
    public static final String CALLED_TO_NAME = "called_to_name";
    public static final String DRIVE_URL = "drive_url";
    public static final String CURRENT_TIME = "current_time";
    public static final String ADDED_THROUGH_CALL_LOG = "added_through_call_log";
    public static final String LOG_DATE_FORMAT = "EEE MMM dd HH:mm:ss zzzz yyyy";
    public static final String CURRENT_USER = "current_user";
    public static final String USERNAME = "username";
    public static final String TIME = "time";
    public static final String TEXT = "text";
    public static final String MESSAGE = "message";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String FULL_NAME = "Full_Name";
    public static final String TASK_LIST = "Task List";
    public static final String VOICE_URL = "voice_url";

    public class Api_URL {
        public static final String BASE_URL = "http://172.105.35.220/appsrv/";
//        public static final String BASE_URL = "http://192.168.137.1:8000/appsrv/";
//        public static final String BASE_URL = "http://192.168.43.68:8000/appsrv/";

        public static final String LOGIN_URL = "loginAgentAndroid";
        public static final String LOGOUT_URL = "logoutAndroid";
        public static final String UPLOAD_LEAD_LOG_FILE_URL = "uploadLeadLogFile";
        public static final String GET_LEAD_NAME_URL = "getLeadName";
        public static final String GET_LEAD_DASHBOARD_URL = "dashboardAgentAndroid";
        public static final String GET_LEAD_INFO_DETAILS_URL = "viewLeadDataAndroid";
        public static final String EDIT_LEAD_DETAILS_URL = "editleaddataAndroid";
        public static final String GET_LATEST_VERSION_OF_APP_URL = "getLatestApkVersion";
        public static final String UPDATE_MISSCALL_DETAIL_URL = "missedCallAndroid";
        public static final String GET_AUDIT_CALL_LOG_URL = "getAuditLogAndroid" ;
        public static final String CREATE_NEW_LEAD_URL = "CreateNewLead";
        public static final String GET_PROFILE_URL = "getProfile";
        public static final String ADD_PROFILE_PIC_URL = "addProfilePic";
        public static final String GET_UNTRACKED_LEAD_DASHBOARD_URL = "dashboardUntracked";
        public static final String GET_LEAD_AGENT_CHAT_URL = "getDiscussions";
        public static final String SEND_MESSAGE_URL = "sendMessageAndroid";
        public static final String GET_UNTRACKED_LEAD_DATA_URL = "viewUntrackedLeadData";
        public static final String GET_SUPPORT_DISCUSSION_URL = "getUserDiscussions";
        public static final String SEND_QUERY_MESSAGE_URL = "sendMessageUser";
        public static final String EDIT_UNTRACKED_LEAD_DETAILS_URL = "editUntrackedData";
        public static final String UPLOAD_DOCUMENT_URL = "addDocuments";
        public static final String GET_DOCUMENTS_URL = "getDocuments";
        public static final String UPLOAD_FROM_CALL_LOG_URL = "uploadFromCallLogFile";
    }

    public static class PermissionCodes {
        public static final int PERMISSION_REQUEST_CODE = 99;
        public static final int CAMERA__REQUEST_CODE = 98;
        public static final int GALLERY_REQUEST_CODE = 97;
        public static final int AADHAR_CARD_REQUEST_CODE = 97;
        public static final int PAN_CARD_REQUEST_CODE = 96;
        public static final int FILE_PICKER_REQUEST_CODE = 95;
    }
}
