package com.dataignyte.bharathabhoomicrm.retrofit.model.response;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LeadAgentConversationResponse {
    @SerializedName(Constants.CURRENT_USER)
    @Expose
    private String current_user;

    @SerializedName("data")
    @Expose
    private ArrayList<ConversationMessage> data;

    public String getCurrent_user() {
        return current_user;
    }

    public ArrayList<ConversationMessage> getData() {
        return data;
    }
}
