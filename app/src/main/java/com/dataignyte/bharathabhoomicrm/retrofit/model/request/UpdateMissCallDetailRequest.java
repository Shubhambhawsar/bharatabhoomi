package com.dataignyte.bharathabhoomicrm.retrofit.model.request;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateMissCallDetailRequest {

    @SerializedName(Constants.USER_EMAIL)
    @Expose
    private String user_email;

    @SerializedName(Constants.CUSTOMER_ID)
    @Expose
    private int customer_id;

    @SerializedName(Constants.PHONE_NUMBER)
    @Expose
    private String phone_number;

    @SerializedName(Constants.CALL_TYPE)
    @Expose
    private String call_type;

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }
}
