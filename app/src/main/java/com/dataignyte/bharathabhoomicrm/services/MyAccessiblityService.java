package com.dataignyte.bharathabhoomicrm.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Toast;

public class MyAccessiblityService extends AccessibilityService {

    private static final String TAG = MyAccessiblityService.class.getSimpleName();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        Log.e(TAG, "onAccessibilityEvent Type: "+accessibilityEvent.getEventType() );
        if (accessibilityEvent.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
            Log.d("myaccess","in window changed");
            AccessibilityNodeInfo info = accessibilityEvent.getSource();
            if (info != null && info.getText() != null) {
                Log.e(TAG, "onAccessibilityEvent Source: "+info.getText() );
            }
        }
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Toast.makeText(this,"Service connected",Toast.LENGTH_SHORT).show();
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        info.notificationTimeout = 0;
        info.packageNames = null;
        setServiceInfo(info);
    }

    @Override
    public void onInterrupt() {

    }
}
