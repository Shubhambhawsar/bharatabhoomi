package com.dataignyte.bharathabhoomicrm.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.BaseActivity;
import com.dataignyte.bharathabhoomicrm.activities.CallerInfoDialogActivity;
import com.dataignyte.bharathabhoomicrm.activities.SaveCallDetailsActivity;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.APICaller.RetrofitClient;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.request.UpdateMissCallDetailRequest;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.SuccessResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneCallsReciever extends BroadcastReceiver {
    private static final String TAG = PhoneCallsReciever.class.getSimpleName();
    private int lastState = TelephonyManager.CALL_STATE_IDLE;
    private Date callStartTime, callEndTime;
    private static boolean isIncoming, wasRinging;
    public static  boolean isCallRecordStarted = false;
    private String savedNumber, number;
    private Context context = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent.getAction() != null)
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
                String info = intent.getExtras().getString("android.intent.extra.NAME");
                String name = intent.getExtras().getString("name");
                callStartTime = new Date();
                if (savedNumber!=null && savedNumber.length()!=0)
                    SharedPref.setSharedPreference(context, Constants.LEAD_NUMBER, savedNumber);
                SharedPref.setSharedPreference(context, Constants.LEAD_NAME, name);
                SharedPref.setSharedPreference(context, Constants.INFO, info);
                SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_OUT_GOING);
                SharedPref.setSharedPreference(context, Constants.START_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callStartTime));
                SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
                sendBroadcast(context);
                context.startActivity(new Intent(context, CallerInfoDialogActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            } else {
                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
                number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                if (number!=null && number.length()!=0)
                    SharedPref.setSharedPreference(context, Constants.LEAD_NUMBER, number);
                Log.e(TAG, "onReceive: " + stateStr+ "  "+ number);
                checkState(stateStr);
            }
    }

    private void checkState(String stateStr) {
        int state = 0;
        if(stateStr!=null) {
            if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                state = TelephonyManager.CALL_STATE_IDLE;
                callEndTime = new Date();
                SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callEndTime));
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    onMissedCall(context, savedNumber, callStartTime);
                    SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_MISSED_CALL);
                    wasRinging = true;
                    callEndTime = new Date();
                    SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callEndTime));
                } else if (isIncoming) {
                    isIncoming = false;
                    if (isCallRecordStarted)
                        onIncomingCallEnded("checkState");
                    else
                        onMissedCall(context, savedNumber, callStartTime);
                } else {
                    onOutgoingCallEnded();
                }
                sendBroadcast(context);
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                state = TelephonyManager.CALL_STATE_RINGING;
                isCallRecordStarted = false;
                SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_INCOMING);
                context.startActivity(new Intent(context, CallerInfoDialogActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            onCallStateChanged(context, state, number);
        }
    }

    public void onCallStateChanged(Context context, int state, String number) {
        if (lastState == state) {
            return;
        }
        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = new Date();
                savedNumber = number;
                wasRinging = true;
                SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_INCOMING);
                SharedPref.setSharedPreference(context, Constants.START_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callStartTime));
                SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
                break;
            case TelephonyManager.CALL_STATE_OFFHOOK:
                if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                    callStartTime = new Date();
                    if (isIncoming) {
                        SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_INCOMING);
                        onIncomingCallStarted(context, number, callStartTime);
                    } else {
                        SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_OUT_GOING);
                        onOutgoingCallStarted(context, savedNumber, callStartTime);
                    }
                    SharedPref.setSharedPreference(context, Constants.START_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callStartTime));
                    SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
                }
                break;
            case TelephonyManager.CALL_STATE_IDLE:
                callEndTime = new Date();
                SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callEndTime));
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    onMissedCall(context, savedNumber, callStartTime);
                    SharedPref.setSharedPreference(context, Constants.CALL_TYPE, Constants.CALL_TYPE_MISSED_CALL);
                    wasRinging = true;
                    callEndTime = new Date();
                    SharedPref.setSharedPreference(context, Constants.END_TIME, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(callEndTime));
                } else if (isIncoming) {
                    isIncoming = false;
                    onIncomingCallEnded("onCallStateChanged");
                } else {
                    onOutgoingCallEnded();
                }
                break;
        }
        lastState = state;
        sendBroadcast(context);
    }

    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        Log.e(TAG, "onIncomingCallStarted: ");
        context.startService(new Intent(context, CallRecordService.class));
    }

    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        Log.e(TAG, "onOutgoingCallStarted: ");
        context.startService(new Intent(context, CallRecordService.class));
    }

    protected void onIncomingCallEnded(String txt) {
        Log.e(TAG, "onIncomingCallEnded: "+txt);
        context.stopService(new Intent(context, CallRecordService.class));
        startSaveCallDetailActivity();
    }

    protected void onOutgoingCallEnded() {
        Log.e(TAG, "onOutgoingCallEnded: ");
        context.stopService(new Intent(context, CallRecordService.class));
        startSaveCallDetailActivity();
    }

    protected void onMissedCall(Context ctx, String number, Date start) {
        Log.e(TAG, "onMissedCall: ");
        callUpdateMissCallDetailsAPI();
    }

    private void startSaveCallDetailActivity(){
        if (SaveCallDetailsActivity.activityFlag)
            ((SaveCallDetailsActivity)SaveCallDetailsActivity.context).finish();
        if (BaseActivity.audiofile==null && SharedPref.getSharedPreferences(context, Constants.USER_ROLE).equalsIgnoreCase(Constants.USER_ROLE_INTERNAL_ASSOCIATE))
            context.startActivity(new Intent(context, SaveCallDetailsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else if (BaseActivity.audiofile!=null)
            context.startActivity(new Intent(context, SaveCallDetailsActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else
            callUpdateMissCallDetailsAPI();
    }

    private void sendBroadcast(Context context) {
        Log.e(PhoneCallsReciever.class.getSimpleName(), "sent Local Broadcast: ");
        Intent intent = new Intent();
        intent.setAction("Call");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void callUpdateMissCallDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            UpdateMissCallDetailRequest object = new UpdateMissCallDetailRequest();
            object.setUser_email(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL));
            object.setCustomer_id(Integer.parseInt(SharedPref.getSharedPreferences(context, Constants.CUSTOMER_ID)));
            object.setPhone_number(SharedPref.getSharedPreferences(context, Constants.LEAD_NUMBER));
            object.setCall_type(Constants.CALL_TYPE_MISSED_CALL);
            RetrofitClient.getAPIService().callUpdateMissCallDetailsAPI(object).enqueue(new Callback<SuccessResponse>() {
                @Override
                public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                    if (response.body() != null && response.body().getSuccess() != null) {
                        Utility.ShowToastMessage(context, response.body().getSuccess());
                    }
                }

                @Override
                public void onFailure(Call<SuccessResponse> call, Throwable t) {
                    Log.e(TAG, "getLeadInfoDetails onFailure: " + t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, context.getResources().getString(R.string.no_internet_connection));
    }

}