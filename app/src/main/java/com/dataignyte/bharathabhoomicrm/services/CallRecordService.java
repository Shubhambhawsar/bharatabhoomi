package com.dataignyte.bharathabhoomicrm.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import com.dataignyte.bharathabhoomicrm.activities.BaseActivity;
import com.dataignyte.bharathabhoomicrm.activities.SaveCallDetailsActivity;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CallRecordService extends Service {
    private static final String TAG = CallRecordService.class.getSimpleName();
    private MediaRecorder recorder;
    private boolean recordstarted = false;

    @Override
    public void onCreate() {
        super.onCreate();
        PhoneCallsReciever.isCallRecordStarted = true;
        startRecord(SharedPref.getSharedPreferences(getApplicationContext(), Constants.LEAD_NUMBER));
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        stopRecord();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    private void startRecord(String fileName) {
        try {
            File sampleDir = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC), "/CallRecordings");
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String file_name = "Record" + "_" + fileName + "_" + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(new Date()) + "_";
            BaseActivity.audiofile = File.createTempFile(file_name, ".mp3", sampleDir);
            recorder = new MediaRecorder();
            recorder.reset();

            if (Build.MANUFACTURER.contains(Constants.MANUFACTURER_ONE_PLUS))
                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
            else
                recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);

            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

            recorder.setOutputFile(BaseActivity.audiofile.getAbsolutePath());

            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            recorder.setAudioSamplingRate(44100);
            recorder.setAudioEncodingBitRate(196000);

            recorder.prepare();
            recorder.start();
            recordstarted = true;
            SaveCallDetailsActivity.callStarted = true;
            Log.e(TAG, "startRecord: " + BaseActivity.audiofile.getAbsolutePath());
        } catch (IOException e) {
            Log.e(TAG, "Exception: " +e.getMessage());
        }
    }

    private void stopRecord() {
        try {
            if (recordstarted) {
                recorder.stop();
                recorder.reset();
                recorder.release();
                recordstarted = false;
                Log.e(TAG, "stopRecord: " + BaseActivity.audiofile.getName() + " Size: " + (BaseActivity.audiofile.getTotalSpace()) + " " + BaseActivity.audiofile.getFreeSpace());
            }
            SharedPref.setSharedPreference(getApplicationContext(), Constants.FILE_PATH, BaseActivity.audiofile.getAbsolutePath());
            SharedPref.setSharedPreference(getApplicationContext(), Constants.FILE_NAME, BaseActivity.audiofile.getName());
            SharedPref.setSharedPreference(getApplicationContext(), Constants.FILE_SIZE, Utility.getStringSizeLengthOfFile(BaseActivity.audiofile.length()));
        } catch (Exception e) {
            Log.e(TAG, "stopRecord Exception : "+e.getMessage() );
        }
    }
}