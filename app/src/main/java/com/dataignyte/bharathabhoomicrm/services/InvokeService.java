package com.dataignyte.bharathabhoomicrm.services;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.SplashActivity;
import com.dataignyte.bharathabhoomicrm.helper.Utility;

public class InvokeService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Utility.checkAndStartOneMinuteAlarm(getApplicationContext());
        createAndShowForegroundNotification(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    private void createAndShowForegroundNotification(Service yourService) {
        final NotificationCompat.Builder builder = getNotificationBuilder(yourService, "com.example.your_app.notification.CHANNEL_ID_FOREGROUND", NotificationManagerCompat.IMPORTANCE_LOW); //Low importance prevent visual appearance for this notification channel on top
        PendingIntent contentIntent = PendingIntent.getActivity(this, 1101 , new Intent(getApplicationContext(), SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK), PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setLargeIcon( BitmapFactory.decodeResource(getResources(), R.drawable.ic_play_logo))
                .setColor(getResources().getColor(R.color.bacgroundColor))
                .setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                .setContentTitle(yourService.getString(R.string.app_name)+ " App")
                .setContentText(yourService.getString(R.string.app_name) + " CRM Application")
                .setContentIntent(contentIntent);
        Notification notification = builder.build();
        yourService.startForeground(1101, notification);
        final NotificationManager nm = (NotificationManager) yourService.getSystemService(Activity.NOTIFICATION_SERVICE);
    }

    public static NotificationCompat.Builder getNotificationBuilder(Context context, String channelId, int importance) {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            prepareChannel(context, channelId, importance);
            builder = new NotificationCompat.Builder(context, channelId);
        } else {
            builder = new NotificationCompat.Builder(context);
        }
        return builder;
    }

    @TargetApi(26)
    private static void prepareChannel(Context context, String id, int importance) {
        final String appName = context.getString(R.string.app_name);
        String description = context.getString(R.string.app_name);
        final NotificationManager nm = (NotificationManager) context.getSystemService(Activity.NOTIFICATION_SERVICE);
        if(nm != null) {
            NotificationChannel nChannel = nm.getNotificationChannel(id);
            if (nChannel == null) {
                nChannel = new NotificationChannel(id, appName, importance);
                nChannel.setDescription(description);
                nm.createNotificationChannel(nChannel);
            }
        }
    }
}