package com.dataignyte.bharathabhoomicrm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.AddLeadActivity;
import com.dataignyte.bharathabhoomicrm.activities.BaseActivity;
import com.dataignyte.bharathabhoomicrm.activities.CallLogActivity;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.CallLogObject;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;

import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = CallLogAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<CallLogObject> callLogsList;
    private Activity activity;
    private ArrayList<CallLogObject> finalArrayList;
    private RecordFilter recordFilter;

    public CallLogAdapter(Context context, ArrayList<CallLogObject> callLogsList, Activity activity) {
        this.context = context;
        this.callLogsList = callLogsList;
        this.finalArrayList = callLogsList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_call_log, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (callLogsList.get(position).getName()!=null)
            holder.txvNumber.setText(callLogsList.get(position).getName());
        else
            holder.txvNumber.setText(callLogsList.get(position).getPhoneNumber());

        holder.txvCallTime.setText(Utility.convertDateFormat(Constants.LOG_DATE_FORMAT, "dd MMM yyy hh:mm a", callLogsList.get(position).getStart_time()));
        holder.txvCallTYpe.setText(callLogsList.get(position).getCall_type());
        holder.txvCallDuration.setText(callLogsList.get(position).getCall_duration()+ " " +context.getString(R.string.sec));

        if (Utility.geNonNullString(callLogsList.get(position).getCall_type()).equalsIgnoreCase(Constants.CALL_TYPE_INCOMING)) {
            holder.txvCallTYpe.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_incoming_call, 0);
            holder.cvBtnSelect.setBackgroundColor(context.getResources().getColor(R.color.transparentGreen));
        } else if (Utility.geNonNullString(callLogsList.get(position).getCall_type()).equalsIgnoreCase(Constants.CALL_TYPE_OUT_GOING)) {
            holder.txvCallTYpe.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_outgoing_call, 0);
            holder.cvBtnSelect.setBackgroundColor(context.getResources().getColor(R.color.transparentSkyBlue));
        } else {
            holder.txvCallTYpe.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_missed_call, 0);
            holder.cvBtnSelect.setBackgroundColor(context.getResources().getColor(R.color.transparentRed));
        }
        holder.cvBtnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activity instanceof CallLogActivity)
                    ((CallLogActivity) activity).getLeadNameAPI(callLogsList.get(position).getName(),
                            callLogsList.get(position).getPhoneNumber(),
                            callLogsList.get(position).getCall_duration(),
                            Utility.convertDateFormat(Constants.LOG_DATE_FORMAT, "dd MMM yyy hh:mm a", callLogsList.get(position).getStart_time()),
                            callLogsList.get(position).getCall_type());
            }
        });
    }

    @Override
    public int getItemCount() {
        if (callLogsList!=null)
            return callLogsList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (recordFilter==null)
            recordFilter = new RecordFilter();

        return recordFilter;
    }


    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint.length()!=0) {
                final ArrayList<CallLogObject> tempFilterList = new ArrayList<CallLogObject>(finalArrayList.size());
                for (int i = 0; i < finalArrayList.size(); i++) {
                    if (Utility.geNonNullString(finalArrayList.get(i).getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } else if (Utility.geNonNullString(finalArrayList.get(i).getPhoneNumber()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    }
                }
                results.values = tempFilterList;
                results.count = tempFilterList.size();
                if (tempFilterList.size()==0) {
                    Utility.ShowToastMessage(context, "No leads found!");
                }
            } else {
                results.values = finalArrayList;
                results.count = finalArrayList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            //it set the data from filter to adapter list and refresh the recyclerview adapter
            callLogsList = (ArrayList<CallLogObject>) results.values;
            notifyDataSetChanged();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cvBtnSelect)
        CardView cvBtnSelect;
        @BindView(R.id.txvNumber)
        TextView txvNumber;
        @BindView(R.id.txvCallTYpe)
        TextView txvCallTYpe;
        @BindView(R.id.txvCallTime)
        TextView txvCallTime;
        @BindView(R.id.txvCallDuration)
        TextView txvCallDuration;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
