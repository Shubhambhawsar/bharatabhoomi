package com.dataignyte.bharathabhoomicrm.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.fragments.LeadDetailsFragment;
import com.dataignyte.bharathabhoomicrm.fragments.UntrackedLeadDetailsFragment;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.LeadInfoData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadInformationAdapter extends RecyclerView.Adapter<LeadInformationAdapter.MyViewHolder> {
    private String TAG = LeadInformationAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<LeadInfoData> arrayList;
    public static boolean setEditable = false;
    private Fragment fragment;

    public LeadInformationAdapter(Context context, Fragment fragment, ArrayList<LeadInfoData> arrayList) {
        this.context = context;
        this.fragment = fragment;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_lead_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int adapterPosition) {
        holder.spnProjects.setEnabled(false);
        holder.spnLeadSource.setEnabled(false);
        holder.txvKey.setText(arrayList.get(adapterPosition).getKey());
        holder.edtValue.setText(arrayList.get(adapterPosition).getValue());

        if (arrayList.get(adapterPosition).getKey().toLowerCase().equalsIgnoreCase("projects")) {
            holder.spnProjects.setVisibility(View.VISIBLE);
            holder.spnLeadSource.setVisibility(View.GONE);
            holder.edtValue.setVisibility(View.GONE);
            if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("BBT"))
                holder.spnProjects.setSelection(1);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("SSC"))
                holder.spnProjects.setSelection(2);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("BBT/SSC"))
                holder.spnProjects.setSelection(3);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Kuberapuram"))
                holder.spnProjects.setSelection(4);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Others"))
                holder.spnProjects.setSelection(5);
            else
                holder.spnProjects.setSelection(0);
        }

        if (arrayList.get(adapterPosition).getKey().toLowerCase().equalsIgnoreCase("Lead_Source")) {
            holder.spnLeadSource.setVisibility(View.VISIBLE);
            holder.spnProjects.setVisibility(View.GONE);
            holder.edtValue.setVisibility(View.GONE);
            if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("SSTV"))
                holder.spnLeadSource.setSelection(1);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Banner"))
                holder.spnLeadSource.setSelection(2);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Broucher distribution"))
                holder.spnLeadSource.setSelection(3);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Pomplet distribution"))
                holder.spnLeadSource.setSelection(4);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Advertisement in Some media"))
                holder.spnLeadSource.setSelection(5);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Website"))
                holder.spnLeadSource.setSelection(6);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("facebook"))
                holder.spnLeadSource.setSelection(7);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("W/A"))
                holder.spnLeadSource.setSelection(8);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Relatives"))
                holder.spnLeadSource.setSelection(9);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Existing client"))
                holder.spnLeadSource.setSelection(10);
            else if (arrayList.get(adapterPosition).getValue().equalsIgnoreCase("Client manager own effort"))
                holder.spnLeadSource.setSelection(11);
            else
                holder.spnLeadSource.setSelection(0);
        }

//        holder.edtValue.setScroller(new Scroller(context));
//        holder.edtValue.setMaxLines(11);
//        holder.edtValue.setVerticalScrollBarEnabled(true);
//        holder.edtValue.setMovementMethod(new ScrollingMovementMethod());

        if (setEditable) {
                holder.edtValue.setFocusable(arrayList.get(adapterPosition).isEditable());
                holder.edtValue.setFocusableInTouchMode(arrayList.get(adapterPosition).isEditable());
                holder.edtValue.setClickable(arrayList.get(adapterPosition).isEditable());

                holder.spnProjects.setEnabled(arrayList.get(adapterPosition).isEditable());
                holder.spnLeadSource.setEnabled(arrayList.get(adapterPosition).isEditable());
        }

        if (!arrayList.get(adapterPosition).isEditable()) {
            holder.edtValue.setFocusable(false);
            holder.edtValue.setTextColor(context.getResources().getColor(R.color.colorTextGrey));
            holder.edtValue.setBackgroundColor(context.getResources().getColor(R.color.backgroundGreyLight));
        }

        holder.edtValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (fragment instanceof  LeadDetailsFragment)
                    ((LeadDetailsFragment) fragment).onValueChanged(adapterPosition, s.toString());
                else
                    ((UntrackedLeadDetailsFragment) fragment).onValueChanged(adapterPosition, s.toString());
            }
        });

        holder.spnProjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.edtValue.setText(holder.spnProjects.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spnLeadSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.edtValue.setText(holder.spnLeadSource.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txvKey)
        TextView txvKey;
        @BindView(R.id.txvValue)
        EditText edtValue;
        @BindView(R.id.spnProjects)
        Spinner spnProjects;
        @BindView(R.id.spnLeadSource)
        Spinner spnLeadSource;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
