package com.dataignyte.bharathabhoomicrm.adapters;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.activities.LeadDetailsActivity;
import com.dataignyte.bharathabhoomicrm.helper.DragEventListener;
import com.dataignyte.bharathabhoomicrm.helper.DragListener;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadsListAdapter extends RecyclerView.Adapter<LeadsListAdapter.MyViewHolder> implements Filterable, View.OnTouchListener {
    private static final String TAG = LeadsListAdapter.class.getSimpleName();
    private final Context context;
    private ArrayList<GetDashBoardResponse> arrayList;
    private ArrayList<GetDashBoardResponse> finalArrayList;
    private RecordFilter recordFilter;
    private DragEventListener dragEventListener;
    public boolean istaskList, isTaskDragable = false;


    public LeadsListAdapter(Context context, ArrayList<GetDashBoardResponse> arrayList, DragEventListener dragEventListener, boolean istaskList) {
        this.context = context;
        this.arrayList = arrayList;
        this.finalArrayList = arrayList;
        this.dragEventListener = dragEventListener;
        this.istaskList = istaskList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_leads_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (!Utility.geNonNullString(arrayList.get(position).getFull_Name()).isEmpty()) {
            String name = arrayList.get(position).getFull_Name().substring(0, 1).toUpperCase() + arrayList.get(position).getFull_Name().substring(1).toLowerCase();
            holder.txvLeadName.setText(name);
        }

        if (Utility.geNonNullString(arrayList.get(position).getEmail()).isEmpty()) {
            holder.llEmail.setVisibility(View.GONE);
        } else {
            holder.llEmail.setVisibility(View.VISIBLE);
            holder.txvEmail.setText(Utility.geNonNullString(arrayList.get(position).getEmail()));
        }

        if (!Utility.geNonNullString(arrayList.get(position).getPhone()).isEmpty()) {
            holder.llNumber.setVisibility(View.VISIBLE);
            holder.ivButtonWhatsapp.setVisibility(View.VISIBLE);
            holder.txvNumber.setText(Utility.geNonNullString(arrayList.get(position).getPhone()));
            holder.ivButtonMessage.setVisibility(View.VISIBLE);
            holder.ivButtonCall.setVisibility(View.VISIBLE);
        } else if (!Utility.geNonNullString(arrayList.get(position).getMobile()).isEmpty()) {
            holder.llNumber.setVisibility(View.VISIBLE);
            holder.ivButtonWhatsapp.setVisibility(View.VISIBLE);
            holder.ivButtonMessage.setVisibility(View.VISIBLE);
            holder.ivButtonCall.setVisibility(View.VISIBLE);
            holder.txvNumber.setText(Utility.geNonNullString(arrayList.get(position).getMobile()));
        } else if (!Utility.geNonNullString(arrayList.get(position).getW_A_number()).isEmpty()) {
            holder.llNumber.setVisibility(View.VISIBLE);
            holder.ivButtonWhatsapp.setVisibility(View.VISIBLE);
            holder.ivButtonMessage.setVisibility(View.VISIBLE);
            holder.ivButtonCall.setVisibility(View.VISIBLE);
            holder.txvNumber.setText(Utility.geNonNullString(arrayList.get(position).getW_A_number()));
        } else {
            holder.llNumber.setVisibility(View.GONE);
        }

        if (Utility.geNonNullString(arrayList.get(position).getLast_contacted_time()).isEmpty()) {
            holder.llLastCall.setVisibility(View.GONE);
        } else {
            holder.llLastCall.setVisibility(View.VISIBLE);
            holder.txvLastCall.setText(Utility.geNonNullString(arrayList.get(position).getLast_contacted_time()));
        }

        holder.ivButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, LeadDetailsActivity.class)
                        .putExtra(Constants._ID, arrayList.get(position).get_id())
                        .putExtra(Constants.LEAD_NAME, arrayList.get(position).getFull_Name().substring(0, 1).toUpperCase() + arrayList.get(position).getFull_Name().substring(1).toLowerCase())
                );
            }
        });

        holder.ivButtonExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ivButtonExpand.setVisibility(View.INVISIBLE);
                holder.llDetails.setVisibility(View.VISIBLE);
                holder.txvButtonHide.setVisibility(View.VISIBLE);
            }
        });

        holder.txvButtonHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ivButtonExpand.setVisibility(View.VISIBLE);
                holder.llDetails.setVisibility(View.GONE);
                holder.txvButtonHide.setVisibility(View.GONE);
            }
        });

        holder.ivButtonWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage(R.string.you_want_to_send_whatsapp)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String number = "";
                                if (!Utility.geNonNullString(arrayList.get(position).getW_A_number()).isEmpty())
                                    number = arrayList.get(position).getW_A_number();
                                else if (!Utility.geNonNullString(arrayList.get(position).getMobile()).isEmpty())
                                    number = arrayList.get(position).getMobile();
                                else if (!Utility.geNonNullString(arrayList.get(position).getPhone()).isEmpty())
                                    number = arrayList.get(position).getPhone();

                                Utility.openWhatsapp(context, number, "");
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            }
        });

        holder.ivButtonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage(R.string.you_want_to_call)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String number = "";
                                if (!Utility.geNonNullString(arrayList.get(position).getPhone()).isEmpty())
                                    number = arrayList.get(position).getPhone();
                                else if (!Utility.geNonNullString(arrayList.get(position).getMobile()).isEmpty())
                                    number = arrayList.get(position).getMobile();
                                else if (!Utility.geNonNullString(arrayList.get(position).getW_A_number()).isEmpty())
                                    number = arrayList.get(position).getW_A_number();

                                try {
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + number));
                                    context.startActivity(intent);
                                } catch (Exception e) {
                                    Log.e(TAG, "onClick: " + e.getMessage());
                                }                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            }
        });

        holder.ivButtonMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage(R.string.you_want_to_send_message)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String number = "";
                                if (!Utility.geNonNullString(arrayList.get(position).getPhone()).isEmpty())
                                    number =  arrayList.get(position).getPhone();
                                else if (!Utility.geNonNullString(arrayList.get(position).getMobile()).isEmpty())
                                    number =  arrayList.get(position).getMobile();
                                else if (!Utility.geNonNullString(arrayList.get(position).getW_A_number()).isEmpty())
                                    number = arrayList.get(position).getW_A_number();

                                    try {
                                    Uri uri = Uri.parse("smsto:" + number);
                                    Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                                    intent.putExtra("sms_body", "");
                                    context.startActivity(intent);
                                } catch (Exception e) {
                                    Log.e(TAG, "onClick: " + e.getMessage());
                                }                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
            }
        });

        holder.cvButtonLeadDetail.setTag(position);
        if (isTaskDragable) {
            holder.cvButtonLeadDetail.setOnTouchListener(this);
            holder.cvButtonLeadDetail.setOnDragListener(new DragListener(context, dragEventListener));
        } else {
            holder.cvButtonLeadDetail.setOnTouchListener(null);
            holder.cvButtonLeadDetail.setOnDragListener(null);
        }
    }

    @Override
    public int getItemCount() {
        if (arrayList!=null)
            return arrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (recordFilter==null)
            recordFilter = new RecordFilter();

        return recordFilter;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, shadowBuilder, v, 0);
                } else {
                    v.startDrag(data, shadowBuilder, v, 0);
                }
                return true;
        }
        return false;
    }

    public ArrayList<GetDashBoardResponse> getList() {
        return arrayList;
    }

    public void updateList(ArrayList<GetDashBoardResponse> list) {
        this.arrayList = list;

        if (istaskList)
            SharedPref.saveTaskListInSharedPreference(context, arrayList);
    }

    public DragListener getDragInstance() {
        if (dragEventListener != null) {
            return new DragListener(context, dragEventListener);
        } else {
            Log.e("ListAdapter", "Listener wasn't initialized!");
            return null;
        }
    }


    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint.length()!=0) {
                final ArrayList<GetDashBoardResponse> tempFilterList = new ArrayList<GetDashBoardResponse>(finalArrayList.size());
                for (int i = 0; i < finalArrayList.size(); i++) {
                        if (Utility.geNonNullString(finalArrayList.get(i).getFull_Name()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getEmail()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getPhone()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getW_A_number()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(String.valueOf(finalArrayList.get(i).get_id())).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        }
                }
                results.values = tempFilterList;
                results.count = tempFilterList.size();
                if (tempFilterList.size()==0) {
                    Utility.ShowToastMessage(context, "No leads found!");
                }
            } else {
                results.values = finalArrayList;
                results.count = finalArrayList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            //it set the data from filter to adapter list and refresh the recyclerview adapter
            arrayList = (ArrayList<GetDashBoardResponse>) results.values;
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txvLeadName)
        TextView txvLeadName;
        @BindView(R.id.ivButtonExpand)
        ImageView ivButtonExpand;
        @BindView(R.id.txvEmail)
        TextView txvEmail;
        @BindView(R.id.llEmail)
        LinearLayout llEmail;
        @BindView(R.id.txvNumber)
        TextView txvNumber;
        @BindView(R.id.llNumber)
        LinearLayout llNumber;
        @BindView(R.id.txvLastCall)
        TextView txvLastCall;
        @BindView(R.id.llLastCall)
        LinearLayout llLastCall;
        @BindView(R.id.txvButtonHide)
        TextView txvButtonHide;
        @BindView(R.id.llDetails)
        LinearLayout llDetails;
        @BindView(R.id.cvButtonLeadDetail)
        CardView cvButtonLeadDetail;
        @BindView(R.id.ivButtonWhatsapp)
        ImageView ivButtonWhatsapp;
        @BindView(R.id.ivButtonCall)
        ImageView ivButtonCall;
        @BindView(R.id.ivButtonMessage)
        ImageView ivButtonMessage;
        @BindView(R.id.ivButtonView)
        ImageView ivButtonView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
