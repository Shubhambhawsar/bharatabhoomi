package com.dataignyte.bharathabhoomicrm.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.ConversationMessage;
import com.dataignyte.bharathabhoomicrm.helper.SharedPref;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadChatMessagAdapter extends RecyclerView.Adapter<LeadChatMessagAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<ConversationMessage> arrayList;
    private boolean isLeadChat;


    public LeadChatMessagAdapter(Context contex, ArrayList<ConversationMessage> arrayList, boolean isLeadChat) {
        this.context = contex;
        this.arrayList = arrayList;
        this.isLeadChat = isLeadChat;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_chat_message, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (Utility.geNonNullString(arrayList.get(position).getUser_email()).equalsIgnoreCase(SharedPref.getSharedPreferences(context, Constants.USER_EMAIL))) {
            holder.cvSentMsgCard.setVisibility(View.VISIBLE);
            holder.txvSentMsgTime.setVisibility(View.VISIBLE);
            holder.ivRecv.setVisibility(View.VISIBLE);

            holder.txvSentMessage.setText(Utility.geNonNullString(arrayList.get(position).getText()));
            holder.txvSentMsgUserRole.setText(Utility.geNonNullString(arrayList.get(position).getUser_role()));
            if (isLeadChat)
                holder.txvSentMsgTime.setText(Utility.geNonNullString(arrayList.get(position).getTime()));
            else
                holder.txvSentMsgTime.setText(Utility.geNonNullString(arrayList.get(position).getDate()));

        } else {
            holder.cvRecvMsgCard.setVisibility(View.VISIBLE);
            holder.txvRecvMsgTime.setVisibility(View.VISIBLE);
            holder.ivSent.setVisibility(View.VISIBLE);

            holder.txvRecvMessage.setText(Utility.geNonNullString(arrayList.get(position).getText()));
            holder.txvRecvMsgUserRole.setText(Utility.geNonNullString(arrayList.get(position).getUser_role()));

            if (isLeadChat)
                holder.txvRecvMsgTime.setText(Utility.geNonNullString(arrayList.get(position).getTime()));
            else
                holder.txvRecvMsgTime.setText(Utility.geNonNullString(arrayList.get(position).getDate()));
        }
    }

    public void updateArrayList(ArrayList<ConversationMessage> updatedArrayList) {
        arrayList = updatedArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txvRecvMsgTime)
        TextView txvRecvMsgTime;
        @BindView(R.id.ivSent)
        ImageView ivSent;
        @BindView(R.id.cvRecvMsgCard)
        CardView cvRecvMsgCard;
        @BindView(R.id.txvRecvMessage)
        TextView txvRecvMessage;
        @BindView(R.id.txvRecvMsgUserRole)
        TextView txvRecvMsgUserRole;

        @BindView(R.id.txvSentMsgTime)
        TextView txvSentMsgTime;
        @BindView(R.id.ivRecv)
        ImageView ivRecv;
        @BindView(R.id.cvSentMsgCard)
        CardView cvSentMsgCard;
        @BindView(R.id.txvSentMessage)
        TextView txvSentMessage;
        @BindView(R.id.txvSentMsgUserRole)
        TextView txvSentMsgUserRole;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
