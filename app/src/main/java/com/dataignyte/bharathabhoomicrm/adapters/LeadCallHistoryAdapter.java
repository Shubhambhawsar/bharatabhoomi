package com.dataignyte.bharathabhoomicrm.adapters;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.bharathabhoomicrm.R;
import com.dataignyte.bharathabhoomicrm.fragments.LeadCallHistoryFragment;
import com.dataignyte.bharathabhoomicrm.fragments.LeadsListFragment;
import com.dataignyte.bharathabhoomicrm.helper.Utility;
import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.CallLogResponse;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeadCallHistoryAdapter extends RecyclerView.Adapter<LeadCallHistoryAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<CallLogResponse> arrayList;
    private Fragment fragment;

    public LeadCallHistoryAdapter(Context context, ArrayList<CallLogResponse> arrayList, Fragment fragment) {
        this.context = context;
        this.arrayList = arrayList;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_lead_call_history, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvCallDuration.setText(arrayList.get(position).getCall_duration());
        holder.txvCalledTo.setText(arrayList.get(position).getCalled_to());
        holder.txvUserName.setText(arrayList.get(position).getUser_name());
        holder.txvUserRole.setText(arrayList.get(position).getUser_role());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txvNote.setText(Html.fromHtml("<b>Note:</b> <br>"+Utility.geNonNullString(arrayList.get(position).getNotes()), Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.txvNote.setText(Html.fromHtml("<b>Note:</b> <br>"+ Utility.geNonNullString(arrayList.get(position).getNotes())));
        }

        if (arrayList.get(position).getCurrent_time()!=null) {
            String [] time = arrayList.get(position).getCurrent_time().split(",");
            holder.txvCallTime.setText(time[0].trim()+"\n"+time[1].trim());
        }

        if (arrayList.get(position).getCall_type() != null) {
            holder.txvCallType.setText(arrayList.get(position).getCall_type());
            if (arrayList.get(position).getCall_type().equalsIgnoreCase(Constants.CALL_TYPE_INCOMING)) {

                holder.rlbackground.setBackgroundColor(context.getResources().getColor(R.color.transparentGreen));
                holder.txvCallType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_incoming_call, 0, 0, 0);

            } else if (arrayList.get(position).getCall_type().equalsIgnoreCase(Constants.CALL_TYPE_OUT_GOING)) {

                holder.rlbackground.setBackgroundColor(context.getResources().getColor(R.color.transparentSkyBlue));
                holder.txvCallType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_outgoing_call, 0, 0, 0);

            } else if (arrayList.get(position).getCall_type().equalsIgnoreCase(Constants.CALL_TYPE_MISSED_CALL)) {

                holder.rlbackground.setBackgroundColor(context.getResources().getColor(R.color.transparentRed));
                holder.txvCallType.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_missed_call, 0, 0, 0);

            }
        }

        holder.ivButtonExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.txvNote.setVisibility(View.VISIBLE);
                holder.txvBtnHide.setVisibility(View.VISIBLE);
                holder.ivButtonExpand.setVisibility(View.GONE);
            }
        });

        holder.txvBtnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.txvNote.setVisibility(View.GONE);
                holder.txvBtnHide.setVisibility(View.GONE);
                holder.ivButtonExpand.setVisibility(View.VISIBLE);
            }
        });

        if (arrayList.get(position).getFile_url()!=null && arrayList.get(position).getFile_url().length()!=0 && !arrayList.get(position).getFile_url().equalsIgnoreCase("None"))
            holder.txvBtnPlayCallRecording.setVisibility(View.VISIBLE);
        else
            holder.txvBtnPlayCallRecording.setVisibility(View.GONE);


        holder.txvBtnPlayCallRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment instanceof LeadCallHistoryFragment)
                    ((LeadCallHistoryFragment) fragment).downloadFile("Call Recording", position, arrayList.get(position).getFile_url());
            }
        });
        
        
        if (arrayList.get(position).getVoice_url()!=null && arrayList.get(position).getVoice_url().length()!=0 && !arrayList.get(position).getVoice_url().equalsIgnoreCase("None"))
            holder.txvBtnPlayVoiceNote.setVisibility(View.VISIBLE);
        else
            holder.txvBtnPlayVoiceNote.setVisibility(View.GONE);


        holder.txvBtnPlayVoiceNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment instanceof LeadCallHistoryFragment)
                    ((LeadCallHistoryFragment) fragment).downloadFile("Voice Note", position, arrayList.get(position).getVoice_url());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txvCallTime)
        TextView txvCallTime;
        @BindView(R.id.txvCallType)
        TextView txvCallType;
        @BindView(R.id.txvCallDuration)
        TextView txvCallDuration;
        @BindView(R.id.txvCalledTo)
        TextView txvCalledTo;
        @BindView(R.id.txvUserName)
        TextView txvUserName;
        @BindView(R.id.txvUserRole)
        TextView txvUserRole;
        @BindView(R.id.ivButtonExpand)
        ImageView ivButtonExpand;
        @BindView(R.id.txvNote)
        TextView txvNote;
        @BindView(R.id.txvBtnHide)
        TextView txvBtnHide;
        @BindView(R.id.cvLogCard)
        CardView cvLogCard;
        @BindView(R.id.ivBtnPlay)
        ImageView ivBtnPlay;
        @BindView(R.id.rlbackground)
        RelativeLayout rlbackground;
        @BindView(R.id.txvBtnPlayVoiceNote)
        TextView txvBtnPlayVoiceNote;
        @BindView(R.id.txvBtnPlayCallRecording)
        TextView txvBtnPlayCallRecording;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
