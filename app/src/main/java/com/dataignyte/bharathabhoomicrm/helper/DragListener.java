package com.dataignyte.bharathabhoomicrm.helper;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.DragEvent;
import android.view.View;

import com.dataignyte.bharathabhoomicrm.adapters.LeadsListAdapter;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;
import com.dataignyte.bharathabhoomicrm.R;
import java.util.ArrayList;
import java.util.List;

public class DragListener implements View.OnDragListener {

    private Context context;
    private boolean isDropped = false;
    private DragEventListener dragEventListener;

    public DragListener(Context context, DragEventListener dragEventListener) {
        this.context = context;
        this.dragEventListener = dragEventListener;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DROP:
                isDropped = true;
                int positionTarget = -1;

                View viewSource = (View) event.getLocalState();
                int viewId = v.getId();
                final int flItem = R.id.cvButtonLeadDetail;
                final int tvEmptyListTop = R.id.txvNoTasksAdded;
                final int tvEmptyListBottom = R.id.txvNoDataFound;
                final int rvTop = R.id.taskListRV;
                final int rvBottom = R.id.recyclerView;

                switch (viewId) {
                    case flItem:
                    case tvEmptyListTop:
                    case tvEmptyListBottom:
                    case rvTop:
                    case rvBottom:

                        RecyclerView target;
                        switch (viewId) {
                            case tvEmptyListTop:
                            case rvTop:
                                target = (RecyclerView) v.getRootView().findViewById(rvTop);
                                break;
                            case tvEmptyListBottom:
                            case rvBottom:
                                target = (RecyclerView) v.getRootView().findViewById(rvBottom);
                                break;
                            default:
                                target = (RecyclerView) v.getParent();
                                positionTarget = (int) v.getTag();
                        }

                        if (viewSource != null) {
                            RecyclerView source = (RecyclerView) viewSource.getParent();

                            LeadsListAdapter adapterSource = (LeadsListAdapter) source.getAdapter();
                            int positionSource = (int) viewSource.getTag();
                            int sourceId = source.getId();

                            GetDashBoardResponse list = adapterSource.getList().get(positionSource);
                            ArrayList<GetDashBoardResponse> listSource = adapterSource.getList();

                            listSource.remove(positionSource);
                            adapterSource.updateList(listSource);
                            adapterSource.notifyDataSetChanged();

                            LeadsListAdapter adapterTarget = (LeadsListAdapter) target.getAdapter();
                            ArrayList<GetDashBoardResponse> customListTarget = adapterTarget.getList();
                            if (positionTarget >= 0) {
                                customListTarget.add(positionTarget, list);
                            } else {
                                customListTarget.add(list);
                            }
                            adapterTarget.updateList(customListTarget);
                            adapterTarget.notifyDataSetChanged();

                            if (sourceId == rvBottom && adapterSource.getItemCount() < 1) {
                                dragEventListener.setEmptyListBottom(true);
                            }
                            if (viewId == tvEmptyListBottom) {
                                dragEventListener.setEmptyListBottom(false);
                            }
                            if (sourceId == rvTop && adapterSource.getItemCount() < 1) {
                                dragEventListener.setEmptyListTop(true);
                            }
                            if (viewId == tvEmptyListTop) {
                                dragEventListener.setEmptyListTop(false);
                            }
                        }
                        break;
                }
                break;
        }

        if (!isDropped && event.getLocalState() != null) {
            ((View) event.getLocalState()).setVisibility(View.VISIBLE);
        }
        return true;
    }
}