package com.dataignyte.bharathabhoomicrm.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.dataignyte.bharathabhoomicrm.retrofit.Constants;
import com.dataignyte.bharathabhoomicrm.retrofit.model.response.GetDashBoardResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPref {

    private static final String TAG = SharedPref.class.getSimpleName();
    private static Context mContext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    public static final String PREFERENCE = "My Shared Prefrence";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public SharedPref(Context mContext) {
        this.mContext = mContext;
        pref = mContext.getSharedPreferences(PREFERENCE, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }


    public static void setSharedPreference(Context context, String name, String value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void setSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        Log.e("setSharedPreference: ", String.valueOf(value));
        editor.putInt(name, value);
        editor.apply();
    }

    public static int getSharedPreferences(Context context, String name, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getInt(name, defaultValue);
    }

    public static String getSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getString(name, "");
    }

    public static void removepreference(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().remove(name).apply();
    }

    public static void clearPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().clear().apply();
    }

    public static void saveTaskListInSharedPreference(Context context, ArrayList<GetDashBoardResponse> arrayList) {
//        if (arrayList != null && arrayList.size() != 0)
//            Utility.ShowToastMessage(context, context.getResources().getString(R.string.cart_item_updated));

        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        String json = new Gson().toJson(arrayList);
        editor.putString(Constants.TASK_LIST, json);
        editor.apply();
        Log.i(TAG, "updatedSharePref: " + arrayList.toString());
    }

    public static ArrayList<GetDashBoardResponse> getSharePreferenceTasksList(Context context) {
        Gson gson = new Gson();
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        String json = settings.getString(Constants.TASK_LIST, null);
        Type type = new TypeToken<ArrayList<GetDashBoardResponse>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

}