package com.dataignyte.bharathabhoomicrm.helper;

public interface DragEventListener {
    void setEmptyListTop(boolean visibility);

    void setEmptyListBottom(boolean visibility);
}